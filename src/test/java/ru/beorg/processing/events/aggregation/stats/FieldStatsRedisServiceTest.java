package ru.beorg.processing.events.aggregation.stats;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import ru.beorg.processing.events.controller.response.ResponseConverter;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.converter.RedisFieldEventKey;
import ru.beorg.processing.events.db.search.field.FieldSearchParameter;

import java.util.Map;

import static java.util.Collections.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.beorg.processing.events.redis.HashNames.FIELD_EVENT;

@RunWith(MockitoJUnitRunner.class)
public class FieldStatsRedisServiceTest {
    @InjectMocks @Spy FieldStatsRedisService subj;

    @Mock FieldSearchParameter searchParameters;
    @Mock RedisTemplate<String, Long> redisTemplate;
    @Mock DateConverter dateConverter;
    @Mock ResponseConverter responseConverter;
    @Mock HashOperations<String, Object, Object> ops;
    @Mock Map<RedisFieldEventKey, Long> events;
    @Mock RedisFieldEventKey redisFieldEventKey;

    @Before
    public void init() {
        when(redisTemplate.opsForHash()).thenReturn(ops);
        when(searchParameters.setByClient(any())).thenReturn(searchParameters);
        when(searchParameters.setByCampaign(any())).thenReturn(searchParameters);
        when(searchParameters.setByEvent(any())).thenReturn(searchParameters);
        when(searchParameters.setByDate(any())).thenReturn(searchParameters);
    }

    @Test
    public void getCurrentDayDocuments() throws Exception {
        subj.getCurrentDayDocuments(searchParameters);

        verify(redisTemplate).opsForHash();
        verify(ops).entries(FIELD_EVENT);
    }

    @Test
    public void countCurrentDayEvents_events_found() throws Exception {
        doReturn(events).when(subj).getCurrentDayDocuments(searchParameters);
        when(events.values()).thenReturn(singleton(5L));

        Long eventCount = subj.countCurrentDayEvents(searchParameters);

        assertEquals(Long.valueOf(5L), eventCount);
    }

    @Test
    public void countCurrentDayEvents_no_events_found() throws Exception {
        doReturn(events).when(subj).getCurrentDayDocuments(searchParameters);
        when(events.values()).thenReturn(EMPTY_LIST);

        Long eventCount = subj.countCurrentDayEvents(searchParameters);

        assertEquals(Long.valueOf(0), eventCount);
    }

    @Test
    public void isRedisSearchable_true() throws Exception {
        when(searchParameters.getUser()).thenReturn(null);
        when(searchParameters.getComment()).thenReturn(null);
        when(searchParameters.getDocument()).thenReturn(null);
        when(searchParameters.getName()).thenReturn(null);

        assertTrue(subj.isRedisSearchable(searchParameters));
    }

    @Test
    public void isRedisSearchable_false_user_present() throws Exception {
        when(searchParameters.getUser()).thenReturn("user@user.test");
        when(searchParameters.getComment()).thenReturn(null);
        when(searchParameters.getDocument()).thenReturn(null);
        when(searchParameters.getName()).thenReturn(null);

        assertFalse(subj.isRedisSearchable(searchParameters));
    }

    @Test
    public void isRedisSearchable_false_comment_present() throws Exception {
        when(searchParameters.getUser()).thenReturn(null);
        when(searchParameters.getComment()).thenReturn("Hohohoho hehehe");
        when(searchParameters.getDocument()).thenReturn(null);
        when(searchParameters.getName()).thenReturn(null);

        assertFalse(subj.isRedisSearchable(searchParameters));
    }

    @Test
    public void isRedisSearchable_false_document_present() throws Exception {
        when(searchParameters.getUser()).thenReturn(null);
        when(searchParameters.getComment()).thenReturn(null);
        when(searchParameters.getDocument()).thenReturn(123);
        when(searchParameters.getName()).thenReturn(null);

        assertFalse(subj.isRedisSearchable(searchParameters));
    }

    @Test
    public void isRedisSearchable_false_name_present() throws Exception {
        when(searchParameters.getUser()).thenReturn(null);
        when(searchParameters.getComment()).thenReturn(null);
        when(searchParameters.getDocument()).thenReturn(null);
        when(searchParameters.getName()).thenReturn(singletonList("field name"));

        assertFalse(subj.isRedisSearchable(searchParameters));
    }

    @Test
    public void shouldBeIncluded() {
        when(searchParameters.getClient()).thenReturn(null);
        when(searchParameters.getCampaign()).thenReturn(null);
        when(searchParameters.getEvent()).thenReturn(null);

        subj.shouldBeIncluded(redisFieldEventKey, searchParameters);

        verify(subj).clientMatches(redisFieldEventKey, searchParameters);
        verify(subj).campaignMatches(redisFieldEventKey, searchParameters);
        verify(subj).eventMatches(redisFieldEventKey, searchParameters);
    }

    @Test
    public void clientMatches_true_null_client_in_search() {
        when(searchParameters.getClient()).thenReturn(null);

        assertTrue(subj.clientMatches(redisFieldEventKey, searchParameters));
    }

    @Test
    public void clientMatches_true_client_present() {
        when(redisFieldEventKey.getClient()).thenReturn(1);
        when(searchParameters.getClient()).thenReturn(singletonList(1));

        assertTrue(subj.clientMatches(redisFieldEventKey, searchParameters));
    }

    @Test
    public void clientMatches_false_client_present() {
        when(redisFieldEventKey.getClient()).thenReturn(2);
        when(searchParameters.getClient()).thenReturn(singletonList(1));

        assertFalse(subj.clientMatches(redisFieldEventKey, searchParameters));
    }

    @Test
    public void campaignMatches_true_null_campaign_in_search() {
        when(searchParameters.getCampaign()).thenReturn(null);

        assertTrue(subj.campaignMatches(redisFieldEventKey, searchParameters));
    }

    @Test
    public void campaignMatches_true_campaign_present() {
        when(redisFieldEventKey.getCampaign()).thenReturn("Test campaign");
        when(searchParameters.getCampaign()).thenReturn(singletonList("Test campaign"));

        assertTrue(subj.campaignMatches(redisFieldEventKey, searchParameters));
    }

    @Test
    public void campaignMatches_false_campaign_present() {
        when(redisFieldEventKey.getCampaign()).thenReturn("Invalid campaign");
        when(searchParameters.getCampaign()).thenReturn(singletonList("Test campaign"));

        assertFalse(subj.campaignMatches(redisFieldEventKey, searchParameters));
    }

    @Test
    public void eventMatches_true_null_event_in_search() {
        when(searchParameters.getEvent()).thenReturn(null);

        assertTrue(subj.eventMatches(redisFieldEventKey, searchParameters));
    }

    @Test
    public void eventMatches_true_event_present() {
        when(redisFieldEventKey.getEvent()).thenReturn(1);
        when(searchParameters.getEvent()).thenReturn(singletonList(1));

        assertTrue(subj.eventMatches(redisFieldEventKey, searchParameters));
    }

    @Test
    public void eventMatches_false_event_present() {
        when(redisFieldEventKey.getEvent()).thenReturn(2);
        when(searchParameters.getEvent()).thenReturn(singletonList(1));

        assertFalse(subj.eventMatches(redisFieldEventKey, searchParameters));
    }
}