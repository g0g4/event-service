package ru.beorg.processing.events.aggregation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import ru.beorg.processing.events.converter.RedisDocumentEventKey;
import ru.beorg.processing.events.db.model.aggregation.DocumentDailyAggregation;
import ru.beorg.processing.events.db.model.aggregation.EventCount;
import ru.beorg.processing.events.db.repository.aggregation.DocumentDailyAggregationRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static ru.beorg.processing.events.redis.HashNames.DOCUMENT_EVENT;

@RunWith(MockitoJUnitRunner.class)
public class UploadDocumentAggregationEventServiceTest {
    @InjectMocks @Spy UploadDocumentAggregationEventService subj;

    @Mock RedisTemplate<String, Long> redisTemplate;
    @Mock RedissonClient redissonClient;
    @Mock RLock lock;
    @Mock HashOperations hashOperations;
    @Mock DocumentDailyAggregationRepository repository;

    Map<String, Long> allDocumentEvents = new HashMap<>();
    Map<String, Long> documentYesterdayEvents = new HashMap<>();

    Map<String, Long> allDocumentEventsWithNullShop = new HashMap<>();
    Map<String, Long> documentYesterdayEventsWithNullShop = new HashMap<>();

    LocalDate yesterday = LocalDate.now().minusDays(1);
    DocumentDailyAggregation documentDailyAggregation = new DocumentDailyAggregation();
    DocumentDailyAggregation documentDailyAggregationWithNullShop = new DocumentDailyAggregation();

    Map<String, Long> eventMap = new HashMap<>();
    List<EventCount> event = new ArrayList<>();
    Map<RedisDocumentEventKey, Map<String, Long>> agg = new HashMap<>();
    Map<RedisDocumentEventKey, Map<String, Long>> aggWithNullShop = new HashMap<>();

    RedisDocumentEventKey documentEventKey = new RedisDocumentEventKey("1<;>TEST<;>1<;>1<;>" + yesterday);
    RedisDocumentEventKey documentEventKeyWithNullShop = new RedisDocumentEventKey("1<;>TEST<;><;>1<;>" + yesterday);

    String redisKey = "1<;>TEST<;>1<;>1<;>" + yesterday;
    String redisKeyWithNullShop = "1<;>TEST<;><;>1<;>" + yesterday;

    @Before
    public void setUp() throws Exception {
        when(redisTemplate.opsForHash()).thenReturn(hashOperations);
        when(redissonClient.getLock(any())).thenReturn(lock);
        when(lock.tryLock()).thenReturn(true);



        allDocumentEvents.put("1<;>TEST<;>1<;>1<;>2017-03-08", 1l);
        allDocumentEvents.put("1<;>TEST<;>1<;>2<;>2017-03-08", 1l);
        allDocumentEvents.put("1<;>TEST<;>1<;>1<;>" + yesterday, 1l);
        allDocumentEvents.put("1<;>TEST<;>1<;>2<;>" + yesterday, 1l);

        documentYesterdayEvents.put("1<;>TEST<;>1<;>2<;>" + yesterday, 1l);
        documentYesterdayEvents.put("1<;>TEST<;>1<;>1<;>" + yesterday, 1l);

        allDocumentEventsWithNullShop.put("1<;>TEST<;><;>1<;>2017-03-08", 1l);
        allDocumentEventsWithNullShop.put("1<;>TEST<;><;>2<;>2017-03-08", 1l);
        allDocumentEventsWithNullShop.put("1<;>TEST<;><;>1<;>" + yesterday, 1l);
        allDocumentEventsWithNullShop.put("1<;>TEST<;><;>2<;>" + yesterday, 1l);

        documentYesterdayEventsWithNullShop.put("1<;>TEST<;><;>1<;>" + yesterday, 1l);
        documentYesterdayEventsWithNullShop.put("1<;>TEST<;><;>2<;>" + yesterday, 1l);

        eventMap.put("1", 1l);
        eventMap.put("2", 1l);
        event.add(new EventCount().setEvent(1).setCount(1L));
        event.add(new EventCount().setEvent(2).setCount(1L));

        documentDailyAggregation
                .setClient(1)
                .setCampaign("TEST")
                .setShop(1)
                .setEvents(event)
                .setDate(yesterday);

        documentDailyAggregationWithNullShop
                .setClient(1)
                .setCampaign("TEST")
                .setShop(null)
                .setEvents(event)
                .setDate(yesterday);

        agg.put(documentEventKey, eventMap);

        aggWithNullShop.put(documentEventKeyWithNullShop, eventMap);
    }

    @Test
    public void uploadDailyAggregation() throws Exception {
        when(hashOperations.entries(DOCUMENT_EVENT)).thenReturn(allDocumentEvents);

        subj.uploadDailyAggregation();

        verify(redisTemplate, times(1)).opsForHash();
        verify(hashOperations, times(1)).entries(DOCUMENT_EVENT);
        verify(subj, times(1)).isOlderEventExists(allDocumentEvents, yesterday);
        verify(subj, times(1)).filterEvent(allDocumentEvents, yesterday);
        verify(subj, times(1)).getDocumentAggregation(documentYesterdayEvents);
        verify(repository, times(1)).save(documentDailyAggregation);
        verify(hashOperations, times(1)).delete(DOCUMENT_EVENT, documentYesterdayEvents.keySet().toArray());
    }

    @Test
    public void uploadDailyAggregationLockFailed() throws Exception {
        when(lock.tryLock()).thenReturn(false);

        subj.uploadDailyAggregation();

        verify(redisTemplate, never()).opsForHash();
        verify(hashOperations, never()).entries(DOCUMENT_EVENT);
        verify(subj, never()).isOlderEventExists(allDocumentEvents, yesterday);
        verify(subj, never()).filterEvent(allDocumentEvents, yesterday);
        verify(subj, never()).getDocumentAggregation(documentYesterdayEvents);
        verify(repository, never()).save(documentDailyAggregation);
        verify(hashOperations, never()).delete(DOCUMENT_EVENT, documentYesterdayEvents.keySet().toArray());
    }

    @Test
    public void uploadDailyAggregationWithNullShop() throws Exception {
        when(hashOperations.entries(DOCUMENT_EVENT)).thenReturn(allDocumentEventsWithNullShop);

        subj.uploadDailyAggregation();

        verify(redisTemplate, times(1)).opsForHash();
        verify(hashOperations, times(1)).entries(DOCUMENT_EVENT);
        verify(subj, times(1)).isOlderEventExists(allDocumentEventsWithNullShop, yesterday);
        verify(subj, times(1)).filterEvent(allDocumentEventsWithNullShop, yesterday);
        verify(subj, times(1)).getDocumentAggregation(documentYesterdayEventsWithNullShop);
        verify(repository, times(1)).save(documentDailyAggregationWithNullShop);
        verify(hashOperations, times(1)).delete(DOCUMENT_EVENT, documentYesterdayEventsWithNullShop.keySet().toArray());
    }

    @Test
    public void getDocumentAggregation() throws Exception {
        Map<RedisDocumentEventKey, Map<String, Long>> aggegation = subj.getDocumentAggregation(documentYesterdayEvents);

        assertEquals(agg, aggegation);
    }

    @Test
    public void getDocumentAggregationWithNullShop() throws Exception {
        Map<RedisDocumentEventKey, Map<String, Long>> aggegation = subj.getDocumentAggregation(documentYesterdayEventsWithNullShop);

        assertEquals(aggWithNullShop, aggegation);
    }

    @Test
    public void keyToEvent() throws Exception {
        String s = subj.keyToEvent(redisKey);

        assertEquals(s, "1");
    }

    @Test
    public void keyToEventWithNullShop() throws Exception {
        String s = subj.keyToEvent(redisKey);

        assertEquals(s, "1");
    }

    @Test
    public void filterEvent() throws Exception {
        Map<String, Long> map = subj.filterEvent(allDocumentEventsWithNullShop, yesterday);

        assertEquals(documentYesterdayEventsWithNullShop, map);
    }

    @Test
    public void isOlderEventExists() throws Exception {
        Boolean isExists = subj.isOlderEventExists(allDocumentEventsWithNullShop, yesterday);
        assertEquals(true, isExists);
    }
}