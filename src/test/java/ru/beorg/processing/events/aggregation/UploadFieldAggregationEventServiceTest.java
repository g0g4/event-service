package ru.beorg.processing.events.aggregation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import ru.beorg.processing.events.converter.RedisFieldEventKey;
import ru.beorg.processing.events.db.model.aggregation.EventCount;
import ru.beorg.processing.events.db.model.aggregation.FieldDailyAggregation;
import ru.beorg.processing.events.db.repository.aggregation.FieldDailyAggregationRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static ru.beorg.processing.events.redis.HashNames.FIELD_EVENT;

@RunWith(MockitoJUnitRunner.class)
public class UploadFieldAggregationEventServiceTest {
    @InjectMocks @Spy UploadFieldAggregationEventService subj;

    @Mock RedisTemplate<String, Long> redisTemplate;
    @Mock RedissonClient redissonClient;
    @Mock RLock lock;
    @Mock HashOperations hashOperations;
    @Mock FieldDailyAggregationRepository repository;

    Map<String, Long> allFieldEvents = new HashMap<>();
    Map<String, Long> fieldYesterdayEvents = new HashMap<>();
    LocalDate yesterday = LocalDate.now().minusDays(1);
    FieldDailyAggregation fieldDailyAggregation = new FieldDailyAggregation();
    Map<String, Long> eventMap = new HashMap<>();
    List<EventCount> event = new ArrayList<>();
    Map<RedisFieldEventKey, Map<String, Long>> agg = new HashMap<>();

    RedisFieldEventKey fieldEventKey = new RedisFieldEventKey("1<;>TEST");

    String redisKey = "1<;>TEST<;>1<;>" + yesterday;

    @Before
    public void setUp() throws Exception {
        when(redisTemplate.opsForHash()).thenReturn(hashOperations);
        when(redissonClient.getLock(any())).thenReturn(lock);
        when(lock.tryLock()).thenReturn(true);

        allFieldEvents.put("1<;>TEST<;>1<;>2017-03-08", 1l);
        allFieldEvents.put("1<;>TEST<;>2<;>2017-03-08", 1l);
        allFieldEvents.put("1<;>TEST<;>1<;>" + yesterday, 1l);
        allFieldEvents.put("1<;>TEST<;>2<;>" + yesterday, 1l);

        fieldYesterdayEvents.put("1<;>TEST<;>1<;>" + yesterday, 1l);
        fieldYesterdayEvents.put("1<;>TEST<;>2<;>" + yesterday, 1l);

        eventMap.put("1", 1l);
        eventMap.put("2", 1l);
        event.add(new EventCount().setEvent(1).setCount(1L));
        event.add(new EventCount().setEvent(2).setCount(1L));

        fieldDailyAggregation
                .setClient(1)
                .setCampaign("TEST")
                .setEvents(event)
                .setDate(yesterday);

        agg.put(fieldEventKey, eventMap);
    }

    @Test
    public void uploadDailyAggregation() throws Exception {
        when(hashOperations.entries(FIELD_EVENT)).thenReturn(allFieldEvents);

        subj.uploadDailyAggregation();

        verify(redisTemplate, times(1)).opsForHash();
        verify(hashOperations, times(1)).entries(FIELD_EVENT);
        verify(subj, times(1)).isOlderEventExists(allFieldEvents, yesterday);
        verify(subj, times(1)).filterEvent(allFieldEvents, yesterday);
        verify(subj, times(1)).getFieldAggregation(fieldYesterdayEvents);
        verify(repository, times(1)).save(fieldDailyAggregation);
        verify(hashOperations, times(1)).delete(FIELD_EVENT, fieldYesterdayEvents.keySet().toArray());
    }

    @Test
    public void uploadDailyAggregationLockFailed() throws Exception {
        when(lock.tryLock()).thenReturn(false);

        subj.uploadDailyAggregation();

        verify(redisTemplate, never()).opsForHash();
        verify(hashOperations, never()).entries(FIELD_EVENT);
        verify(subj, never()).isOlderEventExists(allFieldEvents, yesterday);
        verify(subj, never()).filterEvent(allFieldEvents, yesterday);
        verify(subj, never()).getFieldAggregation(fieldYesterdayEvents);
        verify(repository, never()).save(fieldDailyAggregation);
        verify(hashOperations, never()).delete(FIELD_EVENT, fieldYesterdayEvents.keySet().toArray());
    }

    @Test
    public void getDocumentAggregation() throws Exception {
        Map<RedisFieldEventKey, Map<String, Long>> aggegation = subj.getFieldAggregation(fieldYesterdayEvents);

        assertEquals(agg, aggegation);
    }

    @Test
    public void keyToEvent() throws Exception {
        String s = subj.keyToEvent(redisKey);

        assertEquals(s, "1");
    }

    @Test
    public void filterEvent() throws Exception {
        Map<String, Long> map = subj.filterEvent(allFieldEvents, yesterday);

        assertEquals(fieldYesterdayEvents, map);
    }

    @Test
    public void isOlderEventExists() throws Exception {
        Boolean isExists = subj.isOlderEventExists(allFieldEvents, yesterday);
        assertEquals(true, isExists);
    }
}