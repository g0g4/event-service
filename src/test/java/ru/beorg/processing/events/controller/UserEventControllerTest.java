package ru.beorg.processing.events.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import ru.beorg.processing.events.controller.response.ResponseConverter;
import ru.beorg.processing.events.controller.response.entity.UserEventResponse;
import ru.beorg.processing.events.db.model.event.UserEvent;
import ru.beorg.processing.events.db.repository.event.UserEventRepository;
import ru.beorg.processing.events.db.search.user.UserSearchParameter;
import ru.beorg.processing.events.db.search.user.UserStatsAggregationService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.OK;

@RunWith(MockitoJUnitRunner.class)
public class UserEventControllerTest {
    @InjectMocks @Spy UserEventController subj;

    @Mock UserEventRepository repository;
    @Mock ResponseConverter responseConverter;
    @Mock UserStatsAggregationService statsAggregationService;

    UserSearchParameter searchParameter;
    List<UserEvent> events;

    @Before
    public void init() {
        searchParameter = new UserSearchParameter();
        events = new ArrayList<>();
        events.add(new UserEvent());
    }

    @Test
    public void getDocuments() throws Exception {
        ResponseEntity<List<UserEventResponse>> responseEntity = subj.getDocuments(searchParameter);

        verify(repository).findAll(searchParameter);
        assertNotNull(responseEntity);
        assertEquals(OK, responseEntity.getStatusCode());
    }

    @Test
    public void countDocumentEvents() throws Exception {
        ResponseEntity<Integer> responseEntity = subj.countDocumentEvents(searchParameter);

        verify(statsAggregationService).countEvents(searchParameter);
        assertNotNull(responseEntity);
        assertEquals(Integer.valueOf(0), responseEntity.getBody());
    }

    @Test
    public void deleteDocuments_documents_found() throws Exception {
        when(repository.delete(searchParameter)).thenReturn(1);

        ResponseEntity<Integer> response = subj.deleteDocuments(searchParameter);

        verify(repository).delete(searchParameter);
        assertEquals(OK, response.getStatusCode());
        assertEquals(new Integer(1), response.getBody());
    }

    @Test
    public void deleteDocuments_documents_not_found() throws Exception {
        ResponseEntity<Integer> response = subj.deleteDocuments(searchParameter);

        verify(repository).delete(searchParameter);
        assertEquals(OK, response.getStatusCode());
        assertEquals(new Integer(0), response.getBody());
    }
}