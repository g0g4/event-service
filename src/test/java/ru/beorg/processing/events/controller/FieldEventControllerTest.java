package ru.beorg.processing.events.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import ru.beorg.processing.events.aggregation.stats.FieldStatsRedisService;
import ru.beorg.processing.events.controller.response.ResponseConverter;
import ru.beorg.processing.events.controller.response.entity.FieldEventResponse;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.model.event.FieldEvent;
import ru.beorg.processing.events.db.repository.event.FieldEventRepository;
import ru.beorg.processing.events.db.search.field.FieldSearchParameter;
import ru.beorg.processing.events.db.search.field.FieldStatsAggregationService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.time.LocalDateTime.now;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.OK;

@RunWith(MockitoJUnitRunner.class)
public class FieldEventControllerTest {
    @InjectMocks @Spy FieldEventController subj;

    @Mock FieldEventRepository repository;
    @Mock FieldStatsAggregationService statsAggregationService;
    @Mock FieldStatsRedisService redisService;
    @Mock ResponseConverter responseConverter;
    @Mock DateConverter dateConverter;

    FieldSearchParameter searchParameter;
    List<FieldEvent> events;

    @Before
    public void init() {
        searchParameter = new FieldSearchParameter();
        events = new ArrayList<>();
        events.add(new FieldEvent());
    }

    @Test
    public void getDocuments() throws Exception {
        ResponseEntity<List<FieldEventResponse>> responseEntity = subj.getDocuments(searchParameter);

        verify(repository).findAll(searchParameter);
        assertNotNull(responseEntity);
        assertEquals(OK, responseEntity.getStatusCode());
    }

    @Test
    public void getStats_contains_group_by() throws Exception {
        when(statsAggregationService.containsGroupByClauses(searchParameter)).thenReturn(true);
        when(redisService.isRedisSearchable(searchParameter)).thenReturn(true);

        ResponseEntity responseEntity = subj.getStats(searchParameter);

        verify(statsAggregationService).getStats(searchParameter);
        verify(redisService).getCurrentDayDocuments(searchParameter);
        assertNotNull(responseEntity);
        assertEquals(OK, responseEntity.getStatusCode());
    }

    @Test
    public void getStats_not_contains_group_by() throws Exception {
        doReturn(new ArrayList<>()).when(responseConverter).transformDateToSeconds(any());
        doReturn(new ArrayList<>()).when(redisService).getUngroupedStatsFromRedis(anyMap(), any());

        ResponseEntity responseEntity = subj.getStats(searchParameter);

        verify(statsAggregationService).getStats(searchParameter);
        verify(redisService).getCurrentDayDocuments(searchParameter);
        assertNotNull(responseEntity);
        assertEquals(OK, responseEntity.getStatusCode());
    }

    @Test
    public void countDocumentEvents() throws Exception {
        ResponseEntity<Long> responseEntity = subj.countDocumentEvents(searchParameter);

        verify(statsAggregationService).countEvents(searchParameter);
        assertNotNull(responseEntity);
        assertEquals(Long.valueOf(0), responseEntity.getBody());
    }

    @Test
    public void deleteDocuments_documents_found() throws Exception {
        when(repository.delete(searchParameter)).thenReturn(1);

        ResponseEntity<Integer> response = subj.deleteDocuments(searchParameter);

        verify(repository).delete(searchParameter);
        assertEquals(OK, response.getStatusCode());
        assertEquals(new Integer(1), response.getBody());
    }

    @Test
    public void deleteDocuments_documents_not_found() throws Exception {
        ResponseEntity<Integer> response = subj.deleteDocuments(searchParameter);

        verify(repository).delete(searchParameter);
        assertEquals(OK, response.getStatusCode());
        assertEquals(new Integer(0), response.getBody());
    }

    @Test
    public void containsCurrentDate_true_contains_date() {
        DateConverter realConverter = new DateConverter();
        LocalDateTime currentDate = now();
        searchParameter.setStart(null);
        searchParameter.setEnd(null);
        searchParameter.setDate(realConverter.toEpochSeconds(currentDate));
        when(dateConverter.convertDate(anyLong())).thenReturn(currentDate);

        assertTrue(subj.containsCurrentDate(searchParameter));
    }

    @Test
    public void containsCurrentDate_false_contains_date() {
        DateConverter realConverter = new DateConverter();
        LocalDateTime currentDate = now().minusDays(1);
        searchParameter.setStart(null);
        searchParameter.setEnd(null);
        searchParameter.setDate(realConverter.toEpochSeconds(currentDate));
        when(dateConverter.convertDate(anyLong())).thenReturn(currentDate);

        assertFalse(subj.containsCurrentDate(searchParameter));
    }

}