package ru.beorg.processing.events.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import ru.beorg.processing.events.aggregation.stats.DocumentStatsRedisService;
import ru.beorg.processing.events.controller.response.ResponseConverter;
import ru.beorg.processing.events.controller.response.entity.DocumentEventResponse;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.model.event.DocumentEvent;
import ru.beorg.processing.events.db.repository.event.DocumentEventRepository;
import ru.beorg.processing.events.db.search.document.DocumentSearchParameter;
import ru.beorg.processing.events.db.search.document.DocumentStatsAggregationService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.time.LocalDateTime.now;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.OK;

@RunWith(MockitoJUnitRunner.class)
public class DocumentEventControllerTest {
    @InjectMocks @Spy DocumentEventController subj;

    @Mock DocumentEventRepository repository;
    @Mock DocumentStatsAggregationService statsAggregationService;
    @Mock DocumentStatsRedisService redisService;
    @Mock ResponseConverter responseConverter;
    @Mock DateConverter dateConverter;

    DocumentSearchParameter searchParameter;
    List<DocumentEvent> events;

    @Before
    public void init() {
        searchParameter = new DocumentSearchParameter();
        events = new ArrayList<>();
        events.add(new DocumentEvent());
    }

    @Test
    public void getDocuments() throws Exception {
        ResponseEntity<List<DocumentEventResponse>> responseEntity = subj.getDocuments(searchParameter);

        verify(repository).findAll(searchParameter);
        assertNotNull(responseEntity);
        assertEquals(OK, responseEntity.getStatusCode());
    }

    @Test
    public void getStats_contains_group_by() throws Exception {
        when(statsAggregationService.containsGroupByClauses(searchParameter)).thenReturn(true);
        when(redisService.isRedisSearchable(searchParameter)).thenReturn(true);

        ResponseEntity responseEntity = subj.getStats(searchParameter);

        verify(statsAggregationService).getStats(searchParameter);
        verify(redisService).getCurrentDayDocuments(searchParameter);
        assertNotNull(responseEntity);
        assertEquals(OK, responseEntity.getStatusCode());
    }

    @Test
    public void getStats_not_contains_group_by() throws Exception {
        doReturn(new ArrayList<>()).when(responseConverter).transformDateToSeconds(any());
        doReturn(new ArrayList<>()).when(redisService).getUngroupedStatsFromRedis(anyMap(), any());

        ResponseEntity responseEntity = subj.getStats(searchParameter);

        verify(statsAggregationService).getStats(searchParameter);
        verify(redisService).getCurrentDayDocuments(searchParameter);
        assertNotNull(responseEntity);
        assertEquals(OK, responseEntity.getStatusCode());
    }

    @Test
    public void countDocumentEvents() throws Exception {
        ResponseEntity<Long> responseEntity = subj.countDocumentEvents(searchParameter);

        verify(statsAggregationService).countEvents(searchParameter);
        assertNotNull(responseEntity);
        assertEquals(Long.valueOf(0), responseEntity.getBody());
    }

    @Test
    public void exists_true() throws Exception {
        when(repository.findAll(searchParameter)).thenReturn(events);

        ResponseEntity<Boolean> response = subj.exists(searchParameter);

        verify(repository).findAll(searchParameter);
        assertEquals(OK, response.getStatusCode());
        assertEquals(true, response.getBody());
    }

    @Test
    public void exists_false() throws Exception {
        ResponseEntity<Boolean> response = subj.exists(searchParameter);

        verify(repository).findAll(searchParameter);
        assertEquals(OK, response.getStatusCode());
        assertEquals(false, response.getBody());
    }

    @Test
    public void deleteDocuments_documents_found() throws Exception {
        when(repository.delete(searchParameter)).thenReturn(1);

        ResponseEntity<Integer> response = subj.deleteDocuments(searchParameter);

        verify(repository).delete(searchParameter);
        assertEquals(OK, response.getStatusCode());
        assertEquals(new Integer(1), response.getBody());
    }

    @Test
    public void deleteDocuments_documents_not_found() throws Exception {
        ResponseEntity<Integer> response = subj.deleteDocuments(searchParameter);

        verify(repository).delete(searchParameter);
        assertEquals(OK, response.getStatusCode());
        assertEquals(new Integer(0), response.getBody());
    }

    @Test
    public void containsCurrentDate_true_both_current_date() {
        DateConverter realConverter = new DateConverter();
        LocalDateTime currentDate = now();
        searchParameter.setStart(realConverter.toEpochSeconds(currentDate));
        searchParameter.setEnd(realConverter.toEpochSeconds(currentDate));
        when(dateConverter.convertDate(anyLong())).thenReturn(currentDate);

        assertTrue(subj.containsCurrentDate(searchParameter));
    }

    @Test
    public void containsCurrentDate_true_start_current_date_end_future() {
        DateConverter realConverter = new DateConverter();
        LocalDateTime currentDate = now();
        searchParameter.setStart(realConverter.toEpochSeconds(currentDate));
        searchParameter.setEnd(realConverter.toEpochSeconds(currentDate.plusDays(1)));
        when(dateConverter.convertDate(anyLong())).thenReturn(currentDate);

        assertTrue(subj.containsCurrentDate(searchParameter));
    }

    @Test
    public void containsCurrentDate_true_start_current_date_no_end() {
        DateConverter realConverter = new DateConverter();
        LocalDateTime currentDate = now();
        searchParameter.setStart(realConverter.toEpochSeconds(currentDate));
        searchParameter.setEnd(null);
        when(dateConverter.convertDate(anyLong())).thenReturn(currentDate);

        assertTrue(subj.containsCurrentDate(searchParameter));
    }

    @Test
    public void containsCurrentDate_true_end_current_date() {
        DateConverter realConverter = new DateConverter();
        LocalDateTime currentDate = now();
        searchParameter.setStart(realConverter.toEpochSeconds(currentDate.minusDays(1)));
        searchParameter.setEnd(realConverter.toEpochSeconds(currentDate));
        when(dateConverter.convertDate(anyLong())).thenReturn(currentDate);

        assertTrue(subj.containsCurrentDate(searchParameter));
    }

    @Test
    public void containsCurrentDate_false_end_previous_date() {
        DateConverter realConverter = new DateConverter();
        LocalDateTime previousDate = now().minusDays(1);
        searchParameter.setStart(realConverter.toEpochSeconds(previousDate.minusDays(1)));
        searchParameter.setEnd(realConverter.toEpochSeconds(previousDate));
        when(dateConverter.convertDate(anyLong())).thenReturn(previousDate);

        assertFalse(subj.containsCurrentDate(searchParameter));
    }

    @Test
    public void containsCurrentDate_true_end_current_date_no_start() {
        DateConverter realConverter = new DateConverter();
        LocalDateTime currentDate = now();
        searchParameter.setStart(null);
        searchParameter.setEnd(realConverter.toEpochSeconds(currentDate));
        when(dateConverter.convertDate(anyLong())).thenReturn(currentDate);

        assertTrue(subj.containsCurrentDate(searchParameter));
    }

    @Test
    public void containsCurrentDate_true_end_future_date_no_start() {
        DateConverter realConverter = new DateConverter();
        LocalDateTime futureDate = now().plusDays(1);
        searchParameter.setStart(null);
        searchParameter.setEnd(realConverter.toEpochSeconds(futureDate));
        when(dateConverter.convertDate(anyLong())).thenReturn(futureDate);

        assertTrue(subj.containsCurrentDate(searchParameter));
    }

    @Test
    public void containsCurrentDate_false_no_start() {
        DateConverter realConverter = new DateConverter();
        LocalDateTime previousDate = now().minusDays(1);
        searchParameter.setStart(null);
        searchParameter.setEnd(realConverter.toEpochSeconds(previousDate));
        when(dateConverter.convertDate(anyLong())).thenReturn(previousDate);

        assertFalse(subj.containsCurrentDate(searchParameter));
    }

    @Test
    public void containsCurrentDate_false_no_end() {
        DateConverter realConverter = new DateConverter();
        LocalDateTime futureDate = now().plusDays(1);
        searchParameter.setStart(realConverter.toEpochSeconds(futureDate));
        searchParameter.setEnd(null);
        when(dateConverter.convertDate(anyLong())).thenReturn(futureDate);

        assertFalse(subj.containsCurrentDate(searchParameter));
    }

    @Test
    public void containsCurrentDate_true_both_not_present() {
        searchParameter.setStart(null);
        searchParameter.setEnd(null);

        assertTrue(subj.containsCurrentDate(searchParameter));
    }

}