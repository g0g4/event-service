package ru.beorg.processing.events.listeners;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.model.event.FieldEvent;
import ru.beorg.processing.events.db.repository.event.FieldEventRepository;
import ru.beorg.processing.events.increment.RedisIncrementService;
import ru.beorg.processing.rabbit.common.message.event.FieldEventMessage;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FieldEventMessageListenerTest {
    @InjectMocks @Spy FieldEventListener subj;

    @Mock FieldEventRepository repository;
    @Mock RedisIncrementService redisIncrementService;
    @Mock DateConverter dateConverter;

    FieldEventMessage message = new FieldEventMessage();
    FieldEvent event = new FieldEvent();

    @Before
    public void setUp() throws Exception {
        when(dateConverter.convertDate(1487774663l)).thenReturn(LocalDateTime.parse("2017-02-22T14:44:23"));
        when(repository.save(event)).thenReturn(event);
        message
                .setClient(123)
                .setCampaign("123")
                .setDocument(123)
                .setName("123")
                .setEvent(123)
                .setUser("Test user")
                .setComment("Test comment")
                .setDate(1487774663l);

        event
                .setClient(123)
                .setCampaign("123")
                .setDocument(123)
                .setName("123")
                .setEvent(123)
                .setUser("Test user")
                .setComment("Test comment")
                .setDate(Instant.ofEpochSecond(1487774663l).atZone(ZoneOffset.UTC).toLocalDateTime());
    }

    @Test
    public void saveFieldEvent() throws Exception {
        subj.saveFieldEvent(message);

        verify(repository, times(1)).save(event);
        verify(redisIncrementService, times(1)).increment(event);
    }

    @Test
    public void saveFieldEventWithNoDate() throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();

        when(dateConverter.convertDate(null)).thenReturn(localDateTime);
        message.setDate(null);

        ArgumentCaptor<FieldEvent> captor = ArgumentCaptor.forClass(FieldEvent.class);

        subj.saveFieldEvent(message);

        verify(repository, times(1)).save(captor.capture());

        List<FieldEvent> capturedDocumentEvents = captor.getAllValues();
        LocalDateTime localDateTime1 = capturedDocumentEvents.get(0).getDate();

        assertThat(localDateTime1, Matchers.is(Matchers.greaterThanOrEqualTo(localDateTime)));
    }

    @Test
    public void saveIncorrectFieldMessage() throws Exception {
        message
                .setClient(null)
                .setCampaign(null)
                .setDocument(null)
                .setEvent(null);

        assertEquals(false, subj.isCorrectMessage(message));

        subj.saveFieldEvent(message);

        verify(repository, never()).save(any(FieldEvent.class));
    }
}