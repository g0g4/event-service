package ru.beorg.processing.events.listeners;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.model.event.DocumentEvent;
import ru.beorg.processing.events.db.repository.event.DocumentEventRepository;
import ru.beorg.processing.events.increment.RedisIncrementService;
import ru.beorg.processing.rabbit.common.message.event.DocumentEventMessage;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DocumentEventMessageListenerTest {
    @InjectMocks @Spy DocumentEventListener subj;

    @Mock DocumentEventRepository repository;
    @Mock RedisIncrementService redisIncrementService;
    @Mock DateConverter dateConverter;

    DocumentEventMessage message = new DocumentEventMessage();
    DocumentEvent event = new DocumentEvent();

    @Before
    public void setUp() throws Exception {
        when(dateConverter.convertDate(1487774663l)).thenReturn(LocalDateTime.parse("2017-02-22T14:44:23"));
        when(repository.save(event)).thenReturn(event);
        message
                .setClient(123)
                .setCampaign("123")
                .setShop(123)
                .setDocument(123)
                .setUser("Test user")
                .setEvent(123)
                .setComment("Test comment")
                .setDate(1487774663l);

        event
                .setClient(123)
                .setCampaign("123")
                .setShop(123)
                .setDocument(123)
                .setUser("Test user")
                .setEvent(123)
                .setComment("Test comment")
                .setDate(Instant.ofEpochSecond(1487774663l).atZone(ZoneOffset.UTC).toLocalDateTime());
    }

    @Test
    public void saveDocumentEvent() throws Exception {
        subj.saveDocumentEvent(message);

        verify(repository, times(1)).save(event);
        verify(redisIncrementService, times(1)).increment(event);
    }

    @Test
    public void saveDocumentEventWithNoDate() throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();

        when(dateConverter.convertDate(null)).thenReturn(localDateTime);

        message.setDate(null);
        ArgumentCaptor<DocumentEvent> captor = ArgumentCaptor.forClass(DocumentEvent.class);

        subj.saveDocumentEvent(message);

        verify(repository, times(1)).save(captor.capture());

        List<DocumentEvent> capturedDocumentEvents = captor.getAllValues();
        LocalDateTime localDateTime1 = capturedDocumentEvents.get(0).getDate();

        assertThat(localDateTime1, Matchers.is(Matchers.greaterThanOrEqualTo(localDateTime)));
    }

    @Test
    public void saveIncorrectDocumentMessage() throws Exception {
        message
                .setClient(null)
                .setCampaign(null)
                .setDocument(null)
                .setEvent(null);

        assertEquals(false, subj.isCorrectMessage(message));

        subj.saveDocumentEvent(message);

        verify(repository, never()).save(any(DocumentEvent.class));
    }
}