package ru.beorg.processing.events.listeners;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.model.event.PackEvent;
import ru.beorg.processing.events.db.repository.event.PackEventRepository;
import ru.beorg.processing.rabbit.common.message.event.PackEventMessage;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PackEventListenerTest {
    @InjectMocks PackEventListener subj;

    @Mock PackEventRepository repository;
    @Mock DateConverter dateConverter;

    PackEventMessage message = new PackEventMessage();
    PackEvent event = new PackEvent();

    @Before
    public void setUp() throws Exception {
        when(dateConverter.convertDate(1487774663l)).thenReturn(LocalDateTime.parse("2017-02-22T14:44:23"));
        when(repository.save(event)).thenReturn(event);
        message
                .setClient(123)
                .setCampaign("123")
                .setShop(123)
                .setPack(123)
                .setEvent(123)
                .setUser("Test user")
                .setComment("Test comment")
                .setDate(1487774663l);

        event
                .setClient(123)
                .setCampaign("123")
                .setShop(123)
                .setPack(123)
                .setEvent(123)
                .setUser("Test user")
                .setComment("Test comment")
                .setDate(Instant.ofEpochSecond(1487774663l).atZone(ZoneOffset.UTC).toLocalDateTime());
    }

    @Test
    public void savePackEvent() throws Exception {
        subj.savePackEvent(message);

        verify(repository, times(1)).save(event);
    }

    @Test
    public void savePackEventWithNoDate() throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();

        when(dateConverter.convertDate(null)).thenReturn(localDateTime);

        message.setDate(null);
        ArgumentCaptor<PackEvent> captor = ArgumentCaptor.forClass(PackEvent.class);

        subj.savePackEvent(message);

        verify(repository, times(1)).save(captor.capture());

        List<PackEvent> capturedDocumentEvents = captor.getAllValues();
        LocalDateTime localDateTime1 = capturedDocumentEvents.get(0).getDate();

        assertThat(localDateTime1, Matchers.is(Matchers.greaterThanOrEqualTo(localDateTime)));
    }

    @Test
    public void saveIncorrectFieldMessage() throws Exception {
        message
                .setClient(null)
                .setCampaign(null)
                .setPack(null)
                .setEvent(null);

        assertEquals(false, subj.isCorrectMessage(message));

        subj.savePackEvent(message);

        verify(repository, never()).save(any(PackEvent.class));
    }
}