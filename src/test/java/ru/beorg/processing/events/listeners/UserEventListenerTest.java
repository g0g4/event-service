package ru.beorg.processing.events.listeners;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.model.event.UserEvent;
import ru.beorg.processing.events.db.repository.event.UserEventRepository;
import ru.beorg.processing.rabbit.common.message.event.UserEventMessage;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserEventListenerTest {
    @InjectMocks UserEventListener subj;

    @Mock UserEventRepository repository;
    @Mock DateConverter dateConverter;

    UserEventMessage message = new UserEventMessage();
    UserEvent event = new UserEvent();

    @Before
    public void setUp() throws Exception {
        when(dateConverter.convertDate(1487774663l)).thenReturn(LocalDateTime.parse("2017-02-22T14:44:23"));
        when(repository.save(event)).thenReturn(event);
        message
                .setUser("Test user")
                .setInitiator("123")
                .setEvent(123)
                .setDate(1487774663l);

        event
                .setUser("Test user")
                .setInitiator("123")
                .setEvent(123)
                .setDate(Instant.ofEpochSecond(1487774663l).atZone(ZoneOffset.UTC).toLocalDateTime());
    }

    @Test
    public void saveUserEvent() throws Exception {
        subj.saveUserEvent(message);

        verify(repository, times(1)).save(event);
    }

    @Test
    public void saveUserEventWithNoDate() throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();

        when(dateConverter.convertDate(null)).thenReturn(localDateTime);

        message.setDate(null);
        ArgumentCaptor<UserEvent> captor = ArgumentCaptor.forClass(UserEvent.class);

        subj.saveUserEvent(message);

        verify(repository, times(1)).save(captor.capture());

        List<UserEvent> capturedDocumentEvents = captor.getAllValues();
        LocalDateTime localDateTime1 = capturedDocumentEvents.get(0).getDate();

        assertThat(localDateTime1, Matchers.is(Matchers.greaterThanOrEqualTo(localDateTime)));
    }

    @Test
    public void saveIncorrectFieldMessage() throws Exception {
        message
                .setUser(null)
                .setEvent(null);

        assertEquals(false, subj.isCorrectMessage(message));

        subj.saveUserEvent(message);

        verify(repository, never()).save(any(UserEvent.class));
    }
}