package ru.beorg.processing.events.db.search.user;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.query.Criteria;
import ru.beorg.processing.events.converter.DateConverter;

import java.time.LocalDateTime;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserCriteriaBuilderTest {
    @InjectMocks @Spy UserCriteriaBuilder subj;

    @Mock DateConverter dateConverter;
    @Mock Criteria criteria;
    @Mock UserSearchParameter searchParameter;

    @Before
    public void init() {
        when(dateConverter.convertDate(1487763864L)).thenReturn(LocalDateTime.now());

        when(criteria.and(anyString())).thenReturn(criteria);
        when(criteria.in(anyCollection())).thenReturn(criteria);
        when(criteria.is(anyObject())).thenReturn(criteria);
        when(criteria.gte(anyObject())).thenReturn(criteria);
        when(criteria.lte(anyObject())).thenReturn(criteria);
    }

    @Test
    public void build() {
        when(searchParameter.getDate()).thenReturn(1487763864L);

        Criteria result = subj.build(searchParameter);

        verify(subj).addEventCriteria(any(Criteria.class), any(UserSearchParameter.class));
        verify(subj).addUserCriteria(any(Criteria.class), any(UserSearchParameter.class));
        verify(subj).addInitiatorCriteria(any(Criteria.class), any(UserSearchParameter.class));
        verify(subj).addDateCriteria(any(Criteria.class), any(UserSearchParameter.class));
        assertNotNull(result);
    }

    @Test
    public void addEventCriteria_event_present() {
        subj.addEventCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).in(searchParameter.getEvent());
    }

    @Test
    public void addEventCriteria_event_not_present() {
        when(searchParameter.getEvent()).thenReturn(null);

        subj.addEventCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).in(searchParameter.getEvent());
    }

    @Test
    public void addUserCriteria_user_present() {
        when(searchParameter.getUser()).thenReturn("user@user.com");

        subj.addUserCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).is(searchParameter.getUser());
    }

    @Test
    public void addUserCriteria_user_not_present() {
        when(searchParameter.getUser()).thenReturn(null);

        subj.addUserCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).is(searchParameter.getUser());
    }

    @Test
    public void addInitiatorCriteria_initiator_present() {
        when(searchParameter.getInitiator()).thenReturn("initiator@user.com");

        subj.addInitiatorCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).is(searchParameter.getInitiator());
    }

    @Test
    public void addInitiatorCriteria_initiator_not_present() {
        when(searchParameter.getInitiator()).thenReturn(null);

        subj.addInitiatorCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).is(searchParameter.getInitiator());
    }

    @Test
    public void addDateCriteria_date_present() {
        when(searchParameter.getDate()).thenReturn(1487763864L);

        subj.addDateCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).gte(dateConverter.convertDate(searchParameter.getDate()).toLocalDate().atStartOfDay());
        verify(criteria).lte(
                dateConverter.convertDate(searchParameter.getDate()).toLocalDate().atStartOfDay().plusDays(1));
    }

    @Test
    public void addDateCriteria_date_not_present() {
        when(searchParameter.getDate()).thenReturn(null);

        subj.addDateCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).gte(any());
        verify(criteria, never()).lte(any());
    }

}