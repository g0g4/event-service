package ru.beorg.processing.events.db.search.pack;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.query.Criteria;
import ru.beorg.processing.events.converter.DateConverter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PackCriteriaBuilderTest {

    @InjectMocks @Spy PackCriteriaBuilder subj;

    @Mock DateConverter dateConverter;
    @Mock Criteria criteria;
    @Mock PackSearchParameter searchParameter;

    @Before
    public void init() {
        when(dateConverter.convertDate(1487763864L)).thenReturn(LocalDateTime.now());

        when(criteria.and(anyString())).thenReturn(criteria);
        when(criteria.in(anyCollection())).thenReturn(criteria);
        when(criteria.is(anyObject())).thenReturn(criteria);
        when(criteria.gte(any(LocalDateTime.class))).thenReturn(criteria);
        when(criteria.lte(any(LocalDateTime.class))).thenReturn(criteria);
    }

    @Test
    public void build_date_present() throws Exception {
        when(searchParameter.getDate()).thenReturn(1487763864L);

        Criteria result = subj.build(searchParameter);

        verify(subj).addEventCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addClientCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addCampaignCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addShopCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addPackCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addUserCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addCommentCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addDateCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj, never()).addDateRangeCriteria(any(Criteria.class), any(PackSearchParameter.class));
        assertNotNull(result);
    }

    @Test
    public void build_date_start_end_not_present() throws Exception {
        when(searchParameter.getDate()).thenReturn(null);
        when(searchParameter.getStart()).thenReturn(null);
        when(searchParameter.getEnd()).thenReturn(null);

        Criteria result = subj.build(searchParameter);

        verify(subj).addEventCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addClientCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addCampaignCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addShopCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addPackCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addUserCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addCommentCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj, never()).addDateCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj, never()).addDateRangeCriteria(any(Criteria.class), any(PackSearchParameter.class));
        assertNotNull(result);
    }

    @Test
    public void build_start_end_present() throws Exception {
        when(searchParameter.getDate()).thenReturn(null);
        when(searchParameter.getStart()).thenReturn(1487763864L);
        when(searchParameter.getEnd()).thenReturn(1487763864L);

        Criteria result = subj.build(searchParameter);

        verify(subj).addEventCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addClientCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addCampaignCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addShopCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addPackCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addUserCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addCommentCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj, never()).addDateCriteria(any(Criteria.class), any(PackSearchParameter.class));
        verify(subj).addDateRangeCriteria(any(Criteria.class), any(PackSearchParameter.class));
        assertNotNull(result);
    }

    @Test
    public void addClientCriteria_client_present() throws Exception {
        List<Integer> clientList = new ArrayList<>();
        clientList.add(1);
        when(searchParameter.getClient()).thenReturn(clientList);

        subj.addClientCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).in(searchParameter.getClient());
    }

    @Test
    public void addClientCriteria_client_not_present() throws Exception {
        when(searchParameter.getClient()).thenReturn(null);

        subj.addClientCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).in(searchParameter.getClient());
    }

    @Test
    public void addCampaignCriteria() throws Exception {
        List<String> campaignList = new ArrayList<>();
        campaignList.add("TEST");
        when(searchParameter.getCampaign()).thenReturn(campaignList);

        subj.addCampaignCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).in(searchParameter.getCampaign());

    }

    @Test
    public void addCampaignCriteria_no_campaign() throws Exception {
        when(searchParameter.getCampaign()).thenReturn(null);

        subj.addCampaignCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).in(searchParameter.getCampaign());
    }

    @Test
    public void addShopCriteria_shop_present() throws Exception {
        List<Integer> shopList = new ArrayList<>();
        shopList.add(1);
        when(searchParameter.getShop()).thenReturn(shopList);

        subj.addShopCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).in(searchParameter.getShop());
    }

    @Test
    public void addShopCriteria_shop_not_present() throws Exception {
        when(searchParameter.getShop()).thenReturn(null);

        subj.addShopCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).in(searchParameter.getShop());
    }

    @Test
    public void addUserCriteria_user_present() throws Exception {
        when(searchParameter.getUser()).thenReturn("test");

        subj.addUserCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).is(searchParameter.getUser());
    }

    @Test
    public void addUserCriteria_user_not_present() throws Exception {
        when(searchParameter.getUser()).thenReturn(null);

        subj.addUserCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).is(searchParameter.getUser());
    }

    @Test
    public void addEventCriteria_event_present() throws Exception {
        List<Integer> eventList = new ArrayList<>();
        eventList.add(1);
        when(searchParameter.getEvent()).thenReturn(eventList);

        subj.addEventCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).in(searchParameter.getEvent());
    }

    @Test
    public void addEventCriteria_event_not_present() throws Exception {
        when(searchParameter.getEvent()).thenReturn(null);

        subj.addEventCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).in(searchParameter.getEvent());
    }

    @Test
    public void addCommentCriteria_comment_present() throws Exception {
        when(searchParameter.getComment()).thenReturn("Test comment");

        subj.addCommentCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).is(searchParameter.getComment());
    }

    @Test
    public void addCommentCriteria_comment_not_present() throws Exception {
        when(searchParameter.getComment()).thenReturn(null);

        subj.addCommentCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).is(searchParameter.getComment());
    }

    @Test
    public void addDateCriteria_date_present() throws Exception {
        when(searchParameter.getDate()).thenReturn(1487763864L);
        when(dateConverter.convertDate(1487763864L)).thenReturn(LocalDateTime.now());
        when(criteria.and(anyString())).thenReturn(criteria);

        subj.addDateCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).gte(dateConverter.convertDate(searchParameter.getDate()).toLocalDate().atStartOfDay());
        verify(criteria).lte(dateConverter.convertDate(
                searchParameter.getDate()).toLocalDate().atStartOfDay().plusDays(1));
    }

    @Test
    public void addDateCriteria_date_not_present() throws Exception {
        when(searchParameter.getDate()).thenReturn(null);

        subj.addDateCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).is(dateConverter.convertDate(searchParameter.getDate()));
    }

    @Test
    public void addDateRangeCriteria_start_present() throws Exception {
        when(searchParameter.getStart()).thenReturn(1500L);
        when(searchParameter.getEnd()).thenReturn(null);

        subj.addDateRangeCriteria(criteria, searchParameter);

        verify(criteria).andOperator(any(Criteria.class));
    }

    @Test
    public void addDateRangeCriteria_both_not_present() throws Exception {
        when(searchParameter.getStart()).thenReturn(null);
        when(searchParameter.getEnd()).thenReturn(null);

        subj.addDateRangeCriteria(criteria, searchParameter);

        verify(criteria, never()).andOperator(any(Criteria.class));
    }

    @Test
    public void addPackCriteria_pack_present() throws Exception {
        when(searchParameter.getPack()).thenReturn(new ArrayList<>(Arrays.asList(new Integer[] {1, 2, 3})));

        subj.addPackCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).in(searchParameter.getPack());
    }

    @Test
    public void addPackCriteria_pack_not_present() throws Exception {
        when(searchParameter.getPack()).thenReturn(null);

        subj.addPackCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).in(searchParameter.getPack());
    }

}