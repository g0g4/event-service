package ru.beorg.processing.events.db.search.user;

import com.mongodb.WriteResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import ru.beorg.processing.events.db.model.event.UserEvent;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserEventRepositoryImplTest {

    @InjectMocks @Spy UserEventRepositoryImpl subj;

    @Mock MongoTemplate mongoTemplate;
    @Mock UserCriteriaBuilder criteriaBuilder;
    @Mock UserSearchParameter searchParameter;
    @Mock WriteResult writeResult;

    Criteria criteria;

    @Before
    public void init() {
        criteria = new Criteria();
        when(criteriaBuilder.build(searchParameter)).thenReturn(criteria);
        when(mongoTemplate.remove(Query.query(criteria), UserEvent.class)).thenReturn(writeResult);
        when(writeResult.getN()).thenReturn(0);
    }

    @Test
    public void findAll() throws Exception {
        Criteria criteria = new Criteria();
        when(criteriaBuilder.build(searchParameter)).thenReturn(criteria);

        subj.findAll(searchParameter);

        verify(criteriaBuilder).build(searchParameter);
        verify(mongoTemplate).find(any(), any());
    }

    @Test
    public void delete() throws Exception {
        subj.delete(searchParameter);

        verify(criteriaBuilder).build(searchParameter);
        verify(mongoTemplate).remove(Query.query(criteria), UserEvent.class);
    }

}