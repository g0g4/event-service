package ru.beorg.processing.events.db.search.aggregation;

import com.mongodb.AggregationOutput;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.search.document.DocumentSearchParameter;
import ru.beorg.processing.events.db.search.document.DocumentStatsAggregationService;

import java.util.Iterator;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.beorg.processing.events.db.model.MongoCollectionNames.DOCUMENT_AGGREGATION;

@RunWith(MockitoJUnitRunner.class)
public class AbstractAggregationServiceTest {
    @InjectMocks @Spy DocumentStatsAggregationService subj;

    @Mock MongoTemplate mongoTemplate;
    @Mock DBCollection collection;
    @Mock DBCursor cursor;
    @Mock List<DBObject> searchResults;
    @Mock AggregationOutput aggregationOutput;
    @Mock Iterable<DBObject> aggregationResults;
    @Mock Iterator<DBObject> iterator;
    @Mock DateConverter dateConverter;
    @Mock DocumentSearchParameter searchParameters;

    @Before
    public void init() {
        when(mongoTemplate.getCollection(DOCUMENT_AGGREGATION)).thenReturn(collection);
        when(collection.find(any(), any())).thenReturn(cursor);
        when(cursor.toArray()).thenReturn(searchResults);
        when(collection.aggregate(any())).thenReturn(aggregationOutput);
        when(aggregationOutput.results()).thenReturn(aggregationResults);
        when(aggregationResults.iterator()).thenReturn(iterator);
    }

    @Test
    public void getStats_aggregated() throws Exception {
        when(searchParameters.getByClient()).thenReturn(true);

        subj.getStats(searchParameters);

        verify(subj).getAggregatedStats(searchParameters, DOCUMENT_AGGREGATION);
    }

    @Test
    public void getStats_ungrouped() throws Exception {
        subj.getStats(searchParameters);

        verify(subj).getUngroupedStats(searchParameters, DOCUMENT_AGGREGATION);
    }

    @Test
    public void getAggregatedStats() throws Exception {
        subj.getAggregatedStats(searchParameters, DOCUMENT_AGGREGATION);

        verify(mongoTemplate).getCollection(DOCUMENT_AGGREGATION);
        verify(collection).aggregate(any());
    }

    @Test
    public void getUngroupedStats() throws Exception {
        subj.getUngroupedStats(searchParameters, DOCUMENT_AGGREGATION);

        verify(mongoTemplate).getCollection(DOCUMENT_AGGREGATION);
        verify(collection).find(any(), any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void countEvents_illegal_argument() throws Exception {
        when(searchParameters.getByCampaign()).thenReturn(true);

        subj.countEvents(searchParameters);
    }

    @Test
    public void countEvents() throws Exception {
        subj.countEvents(searchParameters);

        verify(mongoTemplate).getCollection(DOCUMENT_AGGREGATION);
        verify(collection).aggregate(any());
    }

}