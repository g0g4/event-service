package ru.beorg.processing.events.db.search.document;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.query.Criteria;
import ru.beorg.processing.events.converter.DateConverter;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DocumentCriteriaBuilderTest {
    @InjectMocks @Spy DocumentCriteriaBuilder subj;

    @Mock DateConverter dateConverter;
    @Mock Criteria criteria;
    @Mock DocumentSearchParameter searchParameter;

    @Before
    public void init() {
        when(criteria.and(anyString())).thenReturn(criteria);
        when(criteria.in(anyCollection())).thenReturn(criteria);
        when(criteria.is(anyObject())).thenReturn(criteria);
    }

    @Test
    public void build() throws Exception {
        Criteria result = subj.build(searchParameter);

        verify(subj).addClientCriteria(any(Criteria.class), any(DocumentSearchParameter.class));
        verify(subj).addCampaignCriteria(any(Criteria.class), any(DocumentSearchParameter.class));
        verify(subj).addDocumentCriteria(any(Criteria.class), any(DocumentSearchParameter.class));
        verify(subj).addShopCriteria(any(Criteria.class), any(DocumentSearchParameter.class));
        verify(subj).addEventCriteria(any(Criteria.class), any(DocumentSearchParameter.class));
        verify(subj).addCommentCriteria(any(Criteria.class), any(DocumentSearchParameter.class));
        verify(subj).addDateRangeCriteria(any(Criteria.class), any(DocumentSearchParameter.class));
        assertNotNull(result);
    }

    @Test
    public void addClientCriteria_parameter_present() throws Exception {
        List<Integer> clientList = new ArrayList<>();
        clientList.add(1);
        when(searchParameter.getClient()).thenReturn(clientList);

        subj.addClientCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).in(searchParameter.getClient());
    }

    @Test
    public void addClientCriteria_no_client() throws Exception {
        when(searchParameter.getClient()).thenReturn(null);

        subj.addClientCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).in(searchParameter.getClient());
    }

    @Test
    public void addCampaignCriteria() throws Exception {
        List<String> campaignList = new ArrayList<>();
        campaignList.add("TEST");
        when(searchParameter.getCampaign()).thenReturn(campaignList);

        subj.addCampaignCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).in(searchParameter.getCampaign());

    }

    @Test
    public void addCampaignCriteria_no_campaign() throws Exception {
        when(searchParameter.getCampaign()).thenReturn(null);

        subj.addCampaignCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).in(searchParameter.getCampaign());
    }

    @Test
    public void addDocumentCriteria_document_present() throws Exception {
        when(searchParameter.getDocument()).thenReturn(1);

        subj.addDocumentCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).is(searchParameter.getDocument());
    }

    @Test
    public void addDocumentCriteria_document_not_present() throws Exception {
        when(searchParameter.getDocument()).thenReturn(null);

        subj.addDocumentCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).is(searchParameter.getDocument());
    }

    @Test
    public void addShopCriteria_shop_present() throws Exception {
        List<Integer> shopList = new ArrayList<>();
        shopList.add(1);
        when(searchParameter.getShop()).thenReturn(shopList);

        subj.addShopCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).in(searchParameter.getShop());
    }

    @Test
    public void addShopCriteria_shop_not_present() throws Exception {
        when(searchParameter.getShop()).thenReturn(null);

        subj.addShopCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).in(searchParameter.getShop());
    }

    @Test
    public void addEventCriteria_event_present() throws Exception {
        List<Integer> eventList = new ArrayList<>();
        eventList.add(1);
        when(searchParameter.getEvent()).thenReturn(eventList);

        subj.addEventCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).in(searchParameter.getEvent());
    }

    @Test
    public void addEventCriteria_event_not_present() throws Exception {
        when(searchParameter.getEvent()).thenReturn(null);

        subj.addEventCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).in(searchParameter.getEvent());
    }

    @Test
    public void addCommentCriteria_comment_present() throws Exception {
        when(searchParameter.getComment()).thenReturn("Test comment");

        subj.addCommentCriteria(criteria, searchParameter);

        verify(criteria).and(anyString());
        verify(criteria).is(searchParameter.getComment());
    }

    @Test
    public void addCommentCriteria_comment_not_present() throws Exception {
        when(searchParameter.getComment()).thenReturn(null);

        subj.addCommentCriteria(criteria, searchParameter);

        verify(criteria, never()).and(anyString());
        verify(criteria, never()).is(searchParameter.getComment());
    }

    @Test
    public void addDateRangeCriteria_start_present() throws Exception {
        when(searchParameter.getStart()).thenReturn(1500L);
        when(searchParameter.getEnd()).thenReturn(null);

        subj.addDateRangeCriteria(criteria, searchParameter);

        verify(criteria).andOperator(any(Criteria.class));
    }

    @Test
    public void addDateRangeCriteria_both_not_present() throws Exception {
        when(searchParameter.getStart()).thenReturn(null);
        when(searchParameter.getEnd()).thenReturn(null);

        subj.addDateRangeCriteria(criteria, searchParameter);

        verify(criteria, never()).andOperator(any(Criteria.class));
    }

    @Test
    public void addDateRangeCriteria_end_present() throws Exception {
        when(searchParameter.getStart()).thenReturn(null);
        when(searchParameter.getEnd()).thenReturn(1500L);

        subj.addDateRangeCriteria(criteria, searchParameter);

        verify(criteria).andOperator(any(Criteria.class));
    }

}