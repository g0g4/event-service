package ru.beorg.processing.events;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RedissonClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EventsSystemApplicationTests {

    @MockBean private RedissonClient redissonClient;

    @Test
    public void contextLoads() {
    }
}
