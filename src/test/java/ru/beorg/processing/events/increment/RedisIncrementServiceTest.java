package ru.beorg.processing.events.increment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.model.event.DocumentEvent;
import ru.beorg.processing.events.db.model.event.FieldEvent;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static ru.beorg.processing.events.redis.Separator.SEPARATOR;

@RunWith(MockitoJUnitRunner.class)
public class RedisIncrementServiceTest {
    @InjectMocks @Spy RedisIncrementService redisIncrementService;

    @Mock RedisTemplate<String, Long> redisTemplate;
    @Mock HashOperations hashOperations;
    @Mock DateConverter dateConverter;

    DocumentEvent documentEvent = new DocumentEvent();
    FieldEvent fieldEvent = new FieldEvent();
    String documentHash = "documents_daily";
    String fieldHash = "fields_daily";
    String documentRedisKey = "123<;>123<;>123<;>123<;>2017-02-22";
    String fieldRedisKey = "123<;>123<;>123<;>2017-02-22";
    String redisKeyWithNullShop = "123<;>123<;><;>123<;>2017-02-22";

    @Before
    public void setUp() throws Exception {
        when(redisTemplate.opsForHash()).thenReturn(hashOperations);
        when(dateConverter.convertDate(1487774663l)).thenReturn(LocalDateTime.parse("2017-02-22T14:44:23"));
        documentEvent
                .setClient(123)
                .setCampaign("123")
                .setShop(123)
                .setDocument(123)
                .setUser("Test user")
                .setEvent(123)
                .setComment("Test comment")
                .setDate(Instant.ofEpochSecond(1487774663l).atZone(ZoneOffset.UTC).toLocalDateTime());

        fieldEvent
                .setClient(123)
                .setCampaign("123")
                .setDocument(123)
                .setName("123")
                .setEvent(123)
                .setUser("Test user")
                .setComment("Test comment")
                .setDate(Instant.ofEpochSecond(1487774663l).atZone(ZoneOffset.UTC).toLocalDateTime());
    }

    @Test
    public void saveDocumentEvent() throws Exception {
        redisIncrementService.increment(documentEvent);

        verify(redisIncrementService, times(1)).increment(documentEvent);
        verify(redisIncrementService, times(1)).createRedisKey(documentEvent);
        verify(redisIncrementService, times(1)).createOrIncrementRedisKey(documentHash, documentRedisKey);
    }

    @Test
    public void saveFieldEvent() throws Exception {
        redisIncrementService.increment(fieldEvent);

        verify(redisIncrementService, times(1)).increment(fieldEvent);
        verify(redisIncrementService, times(1)).createRedisKey(fieldEvent);
        verify(redisIncrementService, times(1)).createOrIncrementRedisKey(fieldHash, fieldRedisKey);
    }

    @Test
    public void incrementRedisKey(){
        redisIncrementService.createOrIncrementRedisKey(documentHash, documentRedisKey);
        redisIncrementService.createOrIncrementRedisKey(fieldHash, fieldRedisKey);

        verify(redisTemplate, times(2)).opsForHash();
        verify(hashOperations, times(1)).increment(documentHash, documentRedisKey, 1l);
        verify(hashOperations, times(1)).increment(fieldHash, fieldRedisKey, 1l);
    }

    @Test
    public void getRedisKey() throws Exception {
        assertEquals(documentRedisKey, redisIncrementService.createRedisKey(documentEvent));
        assertEquals(fieldRedisKey, redisIncrementService.createRedisKey(fieldEvent));
    }

    @Test
    public void createRedisKeyWithNullShop() throws Exception {
        documentEvent.setShop(null);

        assertEquals(redisKeyWithNullShop, redisIncrementService.createRedisKey(documentEvent));
    }

    @Test
    public void replaceSeparatorIfContains() throws Exception {
        String incorrect = "TE" + SEPARATOR + "ST" + SEPARATOR;

        assertEquals("TEST", redisIncrementService.replaceSeparatorIfContains(incorrect));
    }
}