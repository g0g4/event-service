package ru.beorg.processing.events.converter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class DateConverterTest {
    @InjectMocks DateConverter dateConverter;

    LocalDateTime date;
    Long dateInSeconds;

    @Before
    public void init() {
        date = LocalDateTime.parse("2017-02-22T14:44:23");
        dateInSeconds = 1487774663L;
    }

    @Test
    public void convertDate() throws Exception {
        assertEquals(date, dateConverter.convertDate(dateInSeconds));
    }

    @Test
    public void toEpochSeconds() throws Exception {
        assertEquals(dateInSeconds, dateConverter.toEpochSeconds(date));
    }
}