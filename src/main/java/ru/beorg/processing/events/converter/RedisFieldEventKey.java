package ru.beorg.processing.events.converter;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import static java.lang.Integer.parseInt;
import static ru.beorg.processing.events.redis.FieldKeyPosition.*;
import static ru.beorg.processing.events.redis.Separator.SEPARATOR;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(exclude = {"count"})
public class RedisFieldEventKey implements RedisKey {
    private Integer client;
    private String campaign;
    private Integer event;
    private Long count;

    public RedisFieldEventKey() {

    }

    public RedisFieldEventKey(String redisKey) {
        String[] arr = redisKey.split(SEPARATOR);
        this.client = parseInt(arr[CLIENT]);
        this.campaign = arr[CAMPAIGN];
    }

    public RedisFieldEventKey(String redisKey, Long count) {
        String[] arr = redisKey.split(SEPARATOR);
        this.client = parseInt(arr[CLIENT]);
        this.campaign = arr[CAMPAIGN];
        this.event = (arr[EVENT].equals("")) ? null : parseInt(arr[EVENT]);
        this.count = count;
    }

    public Integer setEvent(Integer event) {
        this.event = event;
        return this.event;
    }

    public Integer setClient(Integer client) {
        this.client = client;
        return this.client;
    }

    public String setCampaign(String campaign) {
        this.campaign = campaign;
        return this.campaign;
    }
}
