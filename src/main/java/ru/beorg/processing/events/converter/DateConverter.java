package ru.beorg.processing.events.converter;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

import static java.time.Instant.ofEpochSecond;
import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.systemDefault;
import static java.time.ZoneOffset.UTC;
import static java.util.Date.from;

@Component
public class DateConverter {

    public LocalDateTime convertDate(Long dateInSeconds) {
        if (dateInSeconds != null) {
            return ofEpochSecond(dateInSeconds).atZone(UTC).toLocalDateTime();
        } else {
            return now();
        }
    }

    public Date toDate(Long dateInSeconds) {
        return from(convertDate(dateInSeconds).atZone(systemDefault()).toInstant());
    }

    public Long toEpochSeconds(Date date) {
        return toEpochSeconds(date.toInstant().atZone(systemDefault()).toLocalDateTime());
    }

    public Long toEpochSeconds(LocalDateTime dateTime) {
        return dateTime.atZone(UTC).toEpochSecond();
    }

}
