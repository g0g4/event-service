package ru.beorg.processing.events.converter;

public interface RedisKey {

    Integer getClient();

    Integer setClient(Integer client);

    String getCampaign();

    String setCampaign(String campaign);

    Integer getEvent();

    Integer setEvent(Integer event);

    Long getCount();

}
