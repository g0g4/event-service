package ru.beorg.processing.events.converter;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import static java.lang.Integer.*;
import static ru.beorg.processing.events.redis.DocumentKeyPosition.*;
import static ru.beorg.processing.events.redis.Separator.SEPARATOR;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(exclude = {"count"})
public class RedisDocumentEventKey implements RedisKey {
    private Integer client;
    private String campaign;
    private Integer shop;
    private Integer event;
    private Long count;

    public RedisDocumentEventKey() {

    }

    public RedisDocumentEventKey(String redisKey) {
        String[] arr = redisKey.split(SEPARATOR);
        this.client = parseInt(arr[CLIENT]);
        this.campaign = arr[CAMPAIGN];
        this.shop = (arr[SHOP].equals("")) ? null : parseInt(arr[SHOP]);
    }

    public RedisDocumentEventKey(String redisKey, Long count) {
        String[] arr = redisKey.split(SEPARATOR);
        this.client = parseInt(arr[CLIENT]);
        this.campaign = arr[CAMPAIGN];
        this.event = (arr[EVENT].equals("")) ? null : parseInt(arr[EVENT]);
        this.shop = (arr[SHOP].equals("")) ? null : parseInt(arr[SHOP]);
        this.count = count;
    }

    public Integer setEvent(Integer event) {
        this.event = event;
        return this.event;
    }

    public Integer setClient(Integer client) {
        this.client = client;
        return this.client;
    }

    public String setCampaign(String campaign) {
        this.campaign = campaign;
        return this.campaign;
    }
}
