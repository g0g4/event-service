package ru.beorg.processing.events.aggregation.stats;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Service;
import ru.beorg.processing.events.controller.response.entity.StatsResponse;
import ru.beorg.processing.events.converter.RedisFieldEventKey;
import ru.beorg.processing.events.db.search.field.FieldSearchParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static java.time.LocalDate.now;
import static java.util.stream.Collectors.toList;
import static ru.beorg.processing.events.controller.response.entity.StatsGroupFieldName.*;
import static ru.beorg.processing.events.redis.HashNames.FIELD_EVENT;

@Service
public class FieldStatsRedisService extends AbstractStatsRedisService<FieldSearchParameter, RedisFieldEventKey> {

    public Map<RedisFieldEventKey, Long> getCurrentDayDocuments(FieldSearchParameter searchParameters) {
        return super.getCurrentDayDocuments(searchParameters, FIELD_EVENT);
    }

    @Override
    public long countCurrentDayEvents(FieldSearchParameter searchParameter) {
        Map<RedisFieldEventKey, Long> redisGroup = getCurrentDayDocuments(
                searchParameter.setByClient(false).setByCampaign(false).setByEvent(false));

        if (redisGroup.values().size() == 1) {
            return redisGroup.values().iterator().next();
        } else {
            return 0;
        }
    }

    @Override
    public boolean isRedisSearchable(FieldSearchParameter searchParameter) {
        return searchParameter.getUser() == null && searchParameter.getComment() == null &&
                searchParameter.getDocument() == null && searchParameter.getName() == null;
    }

    @Override
    public List<StatsResponse> getStatsFromRedis(Map<RedisFieldEventKey, Long> redisGroup,
                                                 FieldSearchParameter searchParameters) {
        List<StatsResponse> stats = new ArrayList<>();

        for (Entry<RedisFieldEventKey, Long> entry : redisGroup.entrySet()) {
            if (shouldBeIncluded(entry.getKey(), searchParameters)) {
                stats.add(responseConverter.convertToStatsResponse(entry.getKey(), entry.getValue(), searchParameters));
            }
        }

        return stats;
    }

    @Override
    public List<DBObject> getUngroupedStatsFromRedis(Map<RedisFieldEventKey, Long> redisGroup, FieldSearchParameter searchParameters) {
        Map<SearchKey, BasicDBObject> stats = new HashMap<>();
        Long dateInSeconds = dateConverter.toEpochSeconds(now().atStartOfDay());

        for (Entry<RedisFieldEventKey, Long> entry : redisGroup.entrySet()) {
            if (shouldBeIncluded(entry.getKey(), searchParameters)) {
                SearchKey key = new SearchKey().setClient(entry.getKey().getClient())
                        .setCampaign(entry.getKey().getCampaign());
                List<BasicDBObject> events = new ArrayList<>();

                if (!stats.containsKey(key)) {
                    events.add(new BasicDBObject()
                            .append("event", entry.getKey().getEvent())
                            .append("count", entry.getValue()));
                    BasicDBObject eventAggregation = new BasicDBObject()
                            .append(CLIENT, entry.getKey().getClient())
                            .append(CAMPAIGN, entry.getKey().getCampaign())
                            .append(DATE, dateInSeconds)
                            .append("events", events);
                    stats.put(key, eventAggregation);
                } else {
                    BasicDBObject eventAggregation = stats.get(key);
                    events = (List<BasicDBObject>) eventAggregation.get("events");
                    events.add(new BasicDBObject()
                            .append("event", entry.getKey().getEvent())
                            .append("count", entry.getValue()));
                }

            }
        }

        return new ArrayList<>(stats.values());
    }

    protected boolean shouldBeIncluded(RedisFieldEventKey key, FieldSearchParameter searchParameters) {
        return clientMatches(key, searchParameters) && campaignMatches(key, searchParameters) &&
                eventMatches(key, searchParameters);
    }

    protected boolean clientMatches(RedisFieldEventKey key, FieldSearchParameter searchParameters) {
        return searchParameters.getClient() == null ||
                searchParameters.getClient().contains(key.getClient());
    }

    protected boolean campaignMatches(RedisFieldEventKey key, FieldSearchParameter searchParameters) {
        return searchParameters.getCampaign() == null ||
                searchParameters.getCampaign().contains(key.getCampaign());
    }

    protected boolean eventMatches(RedisFieldEventKey key, FieldSearchParameter searchParameters) {
        return searchParameters.getEvent() == null ||
                searchParameters.getEvent().contains(key.getEvent());
    }

    @Override
    protected RedisFieldEventKey getEmptyRedisKey() {
        return new RedisFieldEventKey();
    }

    @Override
    protected List<RedisFieldEventKey> toWrapperList(Map<String, Long> events) {
        return events.entrySet()
                .stream()
                .map(e -> new RedisFieldEventKey(e.getKey(), e.getValue()))
                .collect(toList());
    }

    @Data
    @Accessors(chain = true)
    private class SearchKey {
        private Integer client;
        private String campaign;
    }
}
