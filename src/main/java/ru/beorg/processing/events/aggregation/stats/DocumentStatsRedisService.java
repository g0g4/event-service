package ru.beorg.processing.events.aggregation.stats;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Service;
import ru.beorg.processing.events.controller.response.entity.StatsResponse;
import ru.beorg.processing.events.converter.RedisDocumentEventKey;
import ru.beorg.processing.events.db.search.document.DocumentSearchParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static java.time.LocalDate.now;
import static java.util.stream.Collectors.toList;
import static ru.beorg.processing.events.controller.response.entity.StatsGroupFieldName.*;
import static ru.beorg.processing.events.redis.HashNames.DOCUMENT_EVENT;

@Service
public class DocumentStatsRedisService extends AbstractStatsRedisService<DocumentSearchParameter, RedisDocumentEventKey> {

    public Map<RedisDocumentEventKey, Long> getCurrentDayDocuments(DocumentSearchParameter searchParameters) {
        return super.getCurrentDayDocuments(searchParameters, DOCUMENT_EVENT);
    }

    @Override
    public long countCurrentDayEvents(DocumentSearchParameter searchParameter) {
        Map<RedisDocumentEventKey, Long> redisGroup = getCurrentDayDocuments(
                searchParameter.setByClient(false).setByCampaign(false).setByEvent(false));

        if (redisGroup.values().size() == 1) {
            return redisGroup.values().iterator().next();
        } else {
            return 0;
        }
    }

    @Override
    public boolean isRedisSearchable(DocumentSearchParameter searchParameter) {
        return searchParameter.getUser() == null && searchParameter.getComment() == null &&
                searchParameter.getDocument() == null;
    }

    @Override
    public List<StatsResponse> getStatsFromRedis(Map<RedisDocumentEventKey, Long> redisGroup, DocumentSearchParameter searchParameters) {
        List<StatsResponse> stats = new ArrayList<>();

        for (Entry<RedisDocumentEventKey, Long> entry : redisGroup.entrySet()) {
            if (shouldBeIncluded(entry.getKey(), searchParameters)) {
                stats.add(responseConverter.convertToStatsResponse(entry.getKey(), entry.getValue(), searchParameters));
            }
        }

        return stats;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<DBObject> getUngroupedStatsFromRedis(Map<RedisDocumentEventKey, Long> redisGroup, DocumentSearchParameter searchParameters) {
        Map<SearchKey, BasicDBObject> stats = new HashMap<>();
        Long dateInSeconds = dateConverter.toEpochSeconds(now().atStartOfDay());

        for (Entry<RedisDocumentEventKey, Long> entry : redisGroup.entrySet()) {
            if (shouldBeIncluded(entry.getKey(), searchParameters)) {
                SearchKey key = new SearchKey().setClient(entry.getKey().getClient())
                        .setCampaign(entry.getKey().getCampaign())
                        .setShop(entry.getKey().getShop());
                List<BasicDBObject> events = new ArrayList<>();

                if (!stats.containsKey(key)) {
                    events.add(new BasicDBObject()
                            .append("event", entry.getKey().getEvent())
                            .append("count", entry.getValue()));
                    BasicDBObject eventAggregation = new BasicDBObject()
                            .append(CLIENT, entry.getKey().getClient())
                            .append(CAMPAIGN, entry.getKey().getCampaign())
                            .append(SHOP, entry.getKey().getShop())
                            .append(DATE, dateInSeconds)
                            .append("events", events);
                    stats.put(key, eventAggregation);
                } else {
                    BasicDBObject eventAggregation = stats.get(key);
                    events = (List<BasicDBObject>) eventAggregation.get("events");
                    events.add(new BasicDBObject()
                            .append("event", entry.getKey().getEvent())
                            .append("count", entry.getValue()));
                }

            }
        }

        return new ArrayList<>(stats.values());
    }

    protected boolean shouldBeIncluded(RedisDocumentEventKey key, DocumentSearchParameter searchParameters) {
        return clientMatches(key, searchParameters) && campaignMatches(key, searchParameters) &&
                eventMatches(key, searchParameters) && shopMatches(key, searchParameters);
    }

    protected boolean clientMatches(RedisDocumentEventKey key, DocumentSearchParameter searchParameters) {
        return searchParameters.getClient() == null ||
                searchParameters.getClient().contains(key.getClient());
    }

    protected boolean campaignMatches(RedisDocumentEventKey key, DocumentSearchParameter searchParameters) {
        return searchParameters.getCampaign() == null ||
                searchParameters.getCampaign().contains(key.getCampaign());
    }

    protected boolean eventMatches(RedisDocumentEventKey key, DocumentSearchParameter searchParameters) {
        return searchParameters.getEvent() == null ||
                searchParameters.getEvent().contains(key.getEvent());
    }

    protected boolean shopMatches(RedisDocumentEventKey key, DocumentSearchParameter searchParameters) {
        return searchParameters.getShop() == null ||
                searchParameters.getShop().contains(key.getShop());
    }

    @Override
    protected RedisDocumentEventKey getEmptyRedisKey() {
        return new RedisDocumentEventKey();
    }

    @Override
    protected List<RedisDocumentEventKey> toWrapperList(Map<String, Long> events) {
        return events.entrySet()
                .stream()
                .map(e -> new RedisDocumentEventKey(e.getKey(), e.getValue()))
                .collect(toList());
    }

    @Data
    @Accessors(chain = true)
    private class SearchKey {
        private Integer client;
        private String campaign;
        private Integer shop;
    }
}
