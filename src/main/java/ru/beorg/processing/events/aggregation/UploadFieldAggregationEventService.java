package ru.beorg.processing.events.aggregation;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.beorg.processing.common.BaseException;
import ru.beorg.processing.events.converter.RedisFieldEventKey;
import ru.beorg.processing.events.db.model.aggregation.EventCount;
import ru.beorg.processing.events.db.model.aggregation.FieldDailyAggregation;
import ru.beorg.processing.events.db.repository.aggregation.FieldDailyAggregationRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;

import static java.time.LocalDate.now;
import static java.util.Map.Entry;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;
import static ru.beorg.processing.enums.LoggingErrorCode.EV0001;
import static ru.beorg.processing.events.redis.FieldKeyPosition.EVENT;
import static ru.beorg.processing.events.redis.HashNames.FIELD_EVENT;
import static ru.beorg.processing.events.redis.Separator.SEPARATOR;

@Slf4j
@Service
public class UploadFieldAggregationEventService extends RecordEventFilter {

    private static final String FIELD_AGGREGATION_LOCK_NAME = "field_aggregation_lock";

    @Autowired FieldDailyAggregationRepository fieldDailyAggregationRepository;
    @Autowired RedisTemplate<String, Long> redisTemplate;
    @Autowired RedissonClient redissonClient;

    @Scheduled(cron = "${cron.scheduling}")
    public void uploadDailyAggregation() {
        try {
            Lock lock = redissonClient.getLock(FIELD_AGGREGATION_LOCK_NAME);

            if (lock.tryLock()) {
                try {
                    HashOperations<String, String, Long> ops = redisTemplate.opsForHash();
                    Map<String, Long> allFieldEvents = ops.entries(FIELD_EVENT);
                    LocalDate yesterday = now().minusDays(1);

                    if (isOlderEventExists(allFieldEvents, yesterday)) log.warn("There are old records in Redis");

                    Map<String, Long> fieldYesterdayEvents = filterEvent(allFieldEvents, yesterday);

                    if (fieldYesterdayEvents.size() > 0) {
                        Map<RedisFieldEventKey, Map<String, Long>> aggregatedFieldEvents
                                = getFieldAggregation(fieldYesterdayEvents);

                        for (Map.Entry<RedisFieldEventKey, Map<String, Long>> map : aggregatedFieldEvents.entrySet()) {
                            List<EventCount> events = new ArrayList<>();
                            map.getValue().forEach((k, v) -> events.add(new EventCount().setEvent(Integer.valueOf(k)).setCount(v)));

                            FieldDailyAggregation fieldAggregation = new FieldDailyAggregation()
                                    .setClient(map.getKey().getClient())
                                    .setCampaign(map.getKey().getCampaign())
                                    .setEvents(events)
                                    .setDate(yesterday);

                            fieldDailyAggregationRepository.save(fieldAggregation);
                            log.info("Document aggregation {} saved", fieldAggregation);
                        }
                        ops.delete(FIELD_EVENT, fieldYesterdayEvents.keySet().toArray());
                    }
                } finally {
                    lock.unlock();
                }
            }
        } catch (Exception e) {
            throw new BaseException("Field aggregation failed", EV0001, e);
        }
    }

    Map<RedisFieldEventKey, Map<String, Long>> getFieldAggregation(Map<String, Long> map) {
        return map.entrySet()
                .stream()
                .collect(groupingBy(e -> new RedisFieldEventKey(e.getKey()),
                        toMap(e -> keyToEvent(e.getKey()), Entry::getValue)));
    }

    String keyToEvent(String redisKey) {
        return redisKey.split(SEPARATOR)[EVENT];
    }
}
