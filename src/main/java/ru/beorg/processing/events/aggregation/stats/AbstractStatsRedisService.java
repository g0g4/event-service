package ru.beorg.processing.events.aggregation.stats;

import com.mongodb.DBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import ru.beorg.processing.events.aggregation.RecordEventFilter;
import ru.beorg.processing.events.controller.response.ResponseConverter;
import ru.beorg.processing.events.controller.response.entity.StatsResponse;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.converter.RedisKey;
import ru.beorg.processing.events.db.search.RedisGroupableSearchParameter;

import java.util.List;
import java.util.Map;

import static java.time.LocalDate.now;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingLong;
import static ru.beorg.processing.events.controller.response.entity.StatsGroupFieldName.*;

public abstract class AbstractStatsRedisService<E extends RedisGroupableSearchParameter, K extends RedisKey> extends RecordEventFilter {

    @Autowired protected RedisTemplate<String, Long> redisTemplate;
    @Autowired protected DateConverter dateConverter;
    @Autowired protected ResponseConverter responseConverter;

    public Map<K, Long> getCurrentDayDocuments(E searchParameters, String hashName) {
        HashOperations<String, String, Long> ops = redisTemplate.opsForHash();
        Map<String, Long> allDocumentEvents = ops.entries(hashName);

        Map<String, Long> currentDayEvents = filterEvent(allDocumentEvents, now());
        List<K> groups = toWrapperList(currentDayEvents);
        if (containsGroupByClauses(searchParameters)) {
            excludeUnusedGroupings(groups, searchParameters);
        }

        return groups.stream().collect(groupingBy(identity(), summingLong(RedisKey::getCount)));
    }

    public abstract long countCurrentDayEvents(E searchParameters);

    protected abstract List<K> toWrapperList(Map<String, Long> events);

    private boolean containsGroupByClauses(E searchParameters) {
        return (searchParameters.getByClient() != null && searchParameters.getByClient()) ||
                (searchParameters.getByCampaign() != null && searchParameters.getByCampaign()) ||
                (searchParameters.getByDate() != null && searchParameters.getByDate()) ||
                (searchParameters.getByEvent() != null && searchParameters.getByEvent());
    }

    private List<K> excludeUnusedGroupings(List<K> groups, E searchParameters) {
        for (RedisKey groupWrapper : groups) {
            if (searchParameters.getByClient() == null || !searchParameters.getByClient()) {
                groupWrapper.setClient(null);
            }

            if (searchParameters.getByCampaign() == null || !searchParameters.getByCampaign()) {
                groupWrapper.setCampaign(null);
            }

            if (searchParameters.getByEvent() == null || !searchParameters.getByEvent()) {
                groupWrapper.setEvent(null);
            }
        }

        return groups;
    }

    public List<StatsResponse> combineStatistics(List<StatsResponse> statsResponses, Map<K, Long> redisGroup, E searchParameters) {

        for (StatsResponse statsResponse : statsResponses) {
            Map<String, Object> mongoGroup = statsResponse.getGroup();
            K redisKey = getEmptyRedisKey();

            if (mongoGroup.get(CLIENT) != null) {
                redisKey.setClient(((Number) mongoGroup.get(CLIENT)).intValue());
            }

            if (mongoGroup.get(CAMPAIGN) != null) {
                redisKey.setCampaign(mongoGroup.get(CAMPAIGN).toString());
            }

            if (mongoGroup.get(EVENT) != null) {
                redisKey.setEvent(((Number) mongoGroup.get(EVENT)).intValue());
            }

            Long redisEventCount = redisGroup.remove(redisKey);

            if (redisEventCount != null) {
                statsResponse.setCount(statsResponse.getCount() + redisEventCount);
            }
        }

        if (redisGroup.entrySet().size() > 0) {
            statsResponses.addAll(getStatsFromRedis(redisGroup, searchParameters));
        }

        return statsResponses;
    }

    public abstract boolean isRedisSearchable(E searchParameters);

    public abstract List<StatsResponse> getStatsFromRedis(Map<K, Long> redisGroup, E searchParameters);

    public abstract List<DBObject> getUngroupedStatsFromRedis(Map<K, Long> redisGroup, E searchParameters);

    protected abstract K getEmptyRedisKey();

}
