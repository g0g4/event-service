package ru.beorg.processing.events.aggregation;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.beorg.processing.common.BaseException;
import ru.beorg.processing.events.converter.RedisDocumentEventKey;
import ru.beorg.processing.events.db.model.aggregation.DocumentDailyAggregation;
import ru.beorg.processing.events.db.model.aggregation.EventCount;
import ru.beorg.processing.events.db.repository.aggregation.DocumentDailyAggregationRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.Lock;

import static java.time.LocalDate.now;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;
import static ru.beorg.processing.enums.LoggingErrorCode.EV0001;
import static ru.beorg.processing.events.redis.DocumentKeyPosition.EVENT;
import static ru.beorg.processing.events.redis.HashNames.DOCUMENT_EVENT;
import static ru.beorg.processing.events.redis.Separator.SEPARATOR;

@Slf4j
@Service
public class UploadDocumentAggregationEventService extends RecordEventFilter {

    private static final String DOCUMENT_AGGREGATION_LOCK_NAME = "document_aggregation_lock";

    @Autowired DocumentDailyAggregationRepository documentDailyAggregationRepository;
    @Autowired RedisTemplate<String, Long> redisTemplate;
    @Autowired RedissonClient redissonClient;

    @Scheduled(cron = "${cron.scheduling}")
    public void uploadDailyAggregation() {
        try {
            Lock lock = redissonClient.getLock(DOCUMENT_AGGREGATION_LOCK_NAME);

            if (lock.tryLock()) {
                try {
                    HashOperations<String, String, Long> ops = redisTemplate.opsForHash();
                    Map<String, Long> allDocumentEvents = ops.entries(DOCUMENT_EVENT);
                    LocalDate yesterday = now().minusDays(1);

                    if (isOlderEventExists(allDocumentEvents, yesterday)) log.warn("There are old records in Redis");

                    Map<String, Long> documentYesterdayEvents = filterEvent(allDocumentEvents, yesterday);
                    if (documentYesterdayEvents.size() > 0) {
                        Map<RedisDocumentEventKey, Map<String, Long>> aggregatedDocumentEvents
                                = getDocumentAggregation(documentYesterdayEvents);
                        for (Entry<RedisDocumentEventKey, Map<String, Long>> map : aggregatedDocumentEvents.entrySet()) {
                            List<EventCount> events = new ArrayList<>();
                            map.getValue().forEach((k, v) -> events.add(new EventCount().setEvent(Integer.valueOf(k)).setCount(v)));

                            DocumentDailyAggregation documentAggregation = new DocumentDailyAggregation()
                                    .setClient(map.getKey().getClient())
                                    .setCampaign(map.getKey().getCampaign())
                                    .setShop((map.getKey().getShop() != null) ? map.getKey().getShop() : null)
                                    .setEvents(events)
                                    .setDate(yesterday);

                            documentDailyAggregationRepository.save(documentAggregation);
                            log.info("Document aggregation {} saved", documentAggregation);
                        }
                        ops.delete(DOCUMENT_EVENT, documentYesterdayEvents.keySet().toArray());
                    }
                } finally {
                    lock.unlock();
                }
            }
        } catch (Exception e) {
            throw new BaseException("Document aggregation failed", EV0001, e);
        }
    }

    Map<RedisDocumentEventKey, Map<String, Long>> getDocumentAggregation(Map<String, Long> map) {
        return map.entrySet()
                .stream()
                .collect(groupingBy(e -> new RedisDocumentEventKey(e.getKey()),
                        toMap(e -> keyToEvent(e.getKey()), Entry::getValue)));
    }

    String keyToEvent(String redisKey) {
        return redisKey.split(SEPARATOR)[EVENT];
    }
}
