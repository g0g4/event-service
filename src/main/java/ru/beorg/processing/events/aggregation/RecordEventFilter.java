package ru.beorg.processing.events.aggregation;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Map;
import java.util.stream.Collectors;

import static java.time.LocalDate.*;
import static java.util.Map.*;
import static ru.beorg.processing.events.redis.Separator.SEPARATOR;

@Component
public abstract class RecordEventFilter {
    protected Map<String, Long> filterEvent(Map<String, Long> map, LocalDate date) {
        return map.entrySet()
                .stream()
                .filter(e -> parse(keyToDate(e.getKey())).equals(date))
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
    }

    protected Boolean isOlderEventExists(Map<String, Long> map, LocalDate date) {
        return map.entrySet()
                .stream()
                .filter(e -> parse(keyToDate(e.getKey())).isBefore(date))
                .count() > 0;
    }

    private String keyToDate(String redisKey) {
        String[] arr = redisKey.split(SEPARATOR);
        int positionOfDateInRecord = arr.length-1;

        return arr[positionOfDateInRecord];
    }
}
