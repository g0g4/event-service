package ru.beorg.processing.events.controller.response.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserEventResponse {

    private Integer event;
    private String user;
    private String initiator;
    private Long date;

}
