package ru.beorg.processing.events.controller;

public interface RequestPath {

    interface DocumentEvent {
        String GET_DOCUMENTS = "/documents";
        String GET_DOCUMENT_STATS = "/documents/stats";
        String COUNT_DOCUMENT_EVENTS = "/documents/stats/count";
        String DOCUMENT_EXISTS = "/exists/document";
    }

    interface FieldEvent {
        String GET_FIELDS = "/fields";
        String GET_FIELD_STATS = "/fields/stats";
        String COUNT_FIELD_EVENTS = "/fields/stats/count";
    }

    interface PackEvent {
        String GET_PACKS = "/packs";
        String GET_PACK_STATS = "/packs/stats";
        String COUNT_PACK_EVENTS = "/packs/stats/count";
    }

    interface UserEvent {
        String GET_USERS = "/users";
        String GET_USER_STATS = "/users/stats";
        String COUNT_USER_EVENTS = "/users/stats/count";
    }

}
