package ru.beorg.processing.events.controller;

import com.mongodb.DBObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.beorg.processing.events.aggregation.stats.DocumentStatsRedisService;
import ru.beorg.processing.events.controller.response.ResponseConverter;
import ru.beorg.processing.events.controller.response.entity.DocumentEventResponse;
import ru.beorg.processing.events.controller.response.entity.StatsResponse;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.converter.RedisDocumentEventKey;
import ru.beorg.processing.events.db.model.event.DocumentEvent;
import ru.beorg.processing.events.db.repository.event.DocumentEventRepository;
import ru.beorg.processing.events.db.search.document.DocumentSearchParameter;
import ru.beorg.processing.events.db.search.document.DocumentStatsAggregationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.time.LocalDate.now;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static ru.beorg.processing.events.controller.RequestPath.DocumentEvent.*;

@Slf4j
@RestController
public class DocumentEventController {

    @Autowired DocumentEventRepository documentEventRepository;
    @Autowired DocumentStatsAggregationService statsAggregationService;
    @Autowired DocumentStatsRedisService redisService;
    @Autowired ResponseConverter responseConverter;
    @Autowired DateConverter dateConverter;

    @RequestMapping(GET_DOCUMENTS)
    public ResponseEntity<List<DocumentEventResponse>> getDocuments(@RequestBody DocumentSearchParameter searchParameters) {
        log.info("Incoming search request with parameters {}", searchParameters);

        List<DocumentEventResponse> documentEvents = responseConverter.toDocumentEventResponseList(
                documentEventRepository.findAll(searchParameters)
        );

        log.info("Found {} results", documentEvents.size());

        return new ResponseEntity<>(documentEvents, OK);
    }

    @RequestMapping(GET_DOCUMENT_STATS)
    public ResponseEntity getStats(@RequestBody DocumentSearchParameter searchParameters) {
        Iterable<DBObject> stats = statsAggregationService.getStats(searchParameters);

        if (statsAggregationService.containsGroupByClauses(searchParameters)) {
            List<StatsResponse> responses = getCombinedStats(searchParameters, stats);

            log.info("Found: {}", responses);

            return new ResponseEntity<>(responses, OK);
        } else {
            Iterable<DBObject> responses =
                    getCombinedStatsUngrouped(searchParameters, responseConverter.transformDateToSeconds(stats));

            log.info("Found: {}", responses);

            return new ResponseEntity<>(responses, OK);
        }
    }

    @RequestMapping(COUNT_DOCUMENT_EVENTS)
    public ResponseEntity<Long> countDocumentEvents(@RequestBody DocumentSearchParameter searchParameters) {
        Long result = 0L;

        if (redisService.isRedisSearchable(searchParameters) && containsCurrentDate(searchParameters)) {
            result += redisService.countCurrentDayEvents(searchParameters);
        }
        result += statsAggregationService.countEvents(searchParameters);

        return new ResponseEntity<>(result, OK);
    }

    @RequestMapping(DOCUMENT_EXISTS)
    public ResponseEntity<Boolean> exists(@RequestBody DocumentSearchParameter searchParameters) {
        List<DocumentEvent> events = documentEventRepository.findAll(searchParameters);

        if (events.size() > 0) {
            log.info("Document exists with parameters: {}", searchParameters);

            return new ResponseEntity<>(true, OK);
        } else {
            log.info("Document does not exist with parameters: {}", searchParameters);

            return new ResponseEntity<>(false, OK);
        }
    }

    @RequestMapping(value = GET_DOCUMENTS, method = DELETE)
    public ResponseEntity<Integer> deleteDocuments(@RequestBody DocumentSearchParameter searchParameters) {
        log.info("Incoming delete request with parameters {}", searchParameters);

        int deleted = documentEventRepository.delete(searchParameters);

        log.info("Deleted {} documents", deleted);

        return new ResponseEntity<>(deleted, OK);
    }

    private List<StatsResponse> getCombinedStats(DocumentSearchParameter searchParameters, Iterable<DBObject> stats) {
        List<StatsResponse> statsResponses = responseConverter.toStatsResponseList(stats, searchParameters);

        if (redisService.isRedisSearchable(searchParameters) && containsCurrentDate(searchParameters)) {
            Map<RedisDocumentEventKey, Long> redisGroup = redisService.getCurrentDayDocuments(searchParameters);

            if (searchParameters.getByDate() != null && searchParameters.getByDate()) {
                statsResponses.addAll(redisService.getStatsFromRedis(redisGroup, searchParameters));
            } else {
                redisService.combineStatistics(statsResponses, redisGroup, searchParameters);
            }
        }

        return statsResponses;
    }

    private Iterable<DBObject> getCombinedStatsUngrouped(DocumentSearchParameter searchParameters, Iterable<DBObject> stats) {
        List<DBObject> response = new ArrayList<>();

        response.addAll(redisService.getUngroupedStatsFromRedis(redisService.getCurrentDayDocuments(searchParameters), searchParameters));
        stats.forEach(response::add);

        return response;
    }

    protected boolean containsCurrentDate(DocumentSearchParameter searchParameters) {
        return searchParameters.getStart() == null && searchParameters.getEnd() == null ||
                (searchParameters.getStart() == null &&
                        searchParameters.getEnd() != null &&
                        dateConverter.convertDate(searchParameters.getEnd()).toLocalDate().compareTo(now()) >= 0) ||
                (searchParameters.getStart() != null &&
                        dateConverter.convertDate(searchParameters.getStart()).toLocalDate().compareTo(now()) <= 0 &&
                        searchParameters.getEnd() == null) ||
                (searchParameters.getStart() != null &&
                        dateConverter.convertDate(searchParameters.getStart()).toLocalDate().compareTo(now()) <= 0 &&
                        searchParameters.getEnd() != null &&
                        dateConverter.convertDate(searchParameters.getEnd()).toLocalDate().compareTo(now()) >= 0);
    }

}
