package ru.beorg.processing.events.controller.response.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DocumentEventResponse {

    private Integer event;
    private Integer client;
    private String campaign;
    private String comment;
    private Integer document;
    private Integer shop;
    private Long date;
    private String user;

}
