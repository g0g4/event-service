package ru.beorg.processing.events.controller;

import com.mongodb.DBObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.beorg.processing.events.aggregation.stats.FieldStatsRedisService;
import ru.beorg.processing.events.controller.response.ResponseConverter;
import ru.beorg.processing.events.controller.response.entity.FieldEventResponse;
import ru.beorg.processing.events.controller.response.entity.StatsResponse;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.converter.RedisFieldEventKey;
import ru.beorg.processing.events.db.repository.event.FieldEventRepository;
import ru.beorg.processing.events.db.search.field.FieldSearchParameter;
import ru.beorg.processing.events.db.search.field.FieldStatsAggregationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.time.LocalDate.now;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static ru.beorg.processing.events.controller.RequestPath.FieldEvent.*;

@Slf4j
@Controller
public class FieldEventController {

    @Autowired FieldEventRepository fieldEventRepository;
    @Autowired FieldStatsAggregationService statsAggregationService;
    @Autowired FieldStatsRedisService redisService;
    @Autowired ResponseConverter responseConverter;
    @Autowired DateConverter dateConverter;

    @RequestMapping(GET_FIELDS)
    public ResponseEntity<List<FieldEventResponse>> getDocuments(@RequestBody FieldSearchParameter searchParameters) {
        log.info("Incoming search request with parameters {}", searchParameters);

        List<FieldEventResponse> fieldEvents = responseConverter.toFieldEventResponseList(
                fieldEventRepository.findAll(searchParameters)
        );

        log.info("Found {} results", fieldEvents.size());

        return new ResponseEntity<>(fieldEvents, OK);
    }

    @RequestMapping(GET_FIELD_STATS)
    public ResponseEntity getStats(@RequestBody FieldSearchParameter searchParameters) {
        Iterable<DBObject> stats = statsAggregationService.getStats(searchParameters);

        if (statsAggregationService.containsGroupByClauses(searchParameters)) {
            List<StatsResponse> responses = getCombinedStats(searchParameters, stats);

            log.info("Found: {}", responses);

            return new ResponseEntity<>(responses, OK);
        } else {
            Iterable<DBObject> responses =
                    getCombinedStatsUngrouped(searchParameters, responseConverter.transformDateToSeconds(stats));

            log.info("Found: {}", responses);

            return new ResponseEntity<>(responses, OK);
        }
    }

    @RequestMapping(COUNT_FIELD_EVENTS)
    public ResponseEntity<Long> countDocumentEvents(@RequestBody FieldSearchParameter searchParameters) {
        Long result = 0L;

        if (redisService.isRedisSearchable(searchParameters) && containsCurrentDate(searchParameters)) {
            result += redisService.countCurrentDayEvents(searchParameters);
        }
        result += statsAggregationService.countEvents(searchParameters);

        return new ResponseEntity<>(result, OK);
    }

    @RequestMapping(value = GET_FIELDS, method = DELETE)
    public ResponseEntity<Integer> deleteDocuments(@RequestBody FieldSearchParameter searchParameters) {
        log.info("Incoming delete request with parameters {}", searchParameters);

        int deleted = fieldEventRepository.delete(searchParameters);

        log.info("Deleted {} documents", deleted);

        return new ResponseEntity<>(deleted, OK);
    }

    private List<StatsResponse> getCombinedStats(FieldSearchParameter searchParameters, Iterable<DBObject> stats) {
        List<StatsResponse> statsResponses = responseConverter.toStatsResponseList(stats, searchParameters);

        if (redisService.isRedisSearchable(searchParameters) && containsCurrentDate(searchParameters)) {
            Map<RedisFieldEventKey, Long> redisGroup = redisService.getCurrentDayDocuments(searchParameters);

            if (searchParameters.getByDate() != null && searchParameters.getByDate()) {
                statsResponses.addAll(redisService.getStatsFromRedis(redisGroup, searchParameters));
            } else {
                redisService.combineStatistics(statsResponses, redisGroup, searchParameters);
            }
        }

        return statsResponses;
    }

    private Iterable<DBObject> getCombinedStatsUngrouped(FieldSearchParameter searchParameters, Iterable<DBObject> stats) {
        List<DBObject> response = new ArrayList<>();
        response.addAll(redisService.getUngroupedStatsFromRedis(redisService.getCurrentDayDocuments(searchParameters), searchParameters));
        stats.forEach(response::add);

        return response;
    }

    protected boolean containsCurrentDate(FieldSearchParameter searchParameters) {
        return dateContainsCurrentDate(searchParameters) ||
                searchParameters.getDate() == null && startEndIncludeCurrentDate(searchParameters);
    }

    private boolean dateContainsCurrentDate(FieldSearchParameter searchParameters) {
        return searchParameters.getDate() != null &&
                dateConverter.convertDate(searchParameters.getDate()).toLocalDate().equals(now());
    }

    private boolean startEndIncludeCurrentDate(FieldSearchParameter searchParameters) {
        return searchParameters.getStart() == null && searchParameters.getEnd() == null ||
                (searchParameters.getStart() == null &&
                        searchParameters.getEnd() != null &&
                        dateConverter.convertDate(searchParameters.getEnd()).toLocalDate().compareTo(now()) >= 0) ||
                (searchParameters.getStart() != null &&
                        dateConverter.convertDate(searchParameters.getStart()).toLocalDate().compareTo(now()) <= 0 &&
                        searchParameters.getEnd() == null) ||
                (searchParameters.getStart() != null &&
                        dateConverter.convertDate(searchParameters.getStart()).toLocalDate().compareTo(now()) <= 0 &&
                        searchParameters.getEnd() != null &&
                        dateConverter.convertDate(searchParameters.getEnd()).toLocalDate().compareTo(now()) >= 0);
    }

}
