package ru.beorg.processing.events.controller.response.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

@Data
@Accessors(chain = true)
public class StatsResponse {

    private Long count;
    private Map<String, Object> group;

}
