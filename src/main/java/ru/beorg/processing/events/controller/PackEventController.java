package ru.beorg.processing.events.controller;

import com.mongodb.DBObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.beorg.processing.events.controller.response.ResponseConverter;
import ru.beorg.processing.events.controller.response.entity.PackEventResponse;
import ru.beorg.processing.events.db.repository.event.PackEventRepository;
import ru.beorg.processing.events.db.search.pack.PackSearchParameter;
import ru.beorg.processing.events.db.search.pack.PackStatsAggregationService;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static ru.beorg.processing.events.controller.RequestPath.PackEvent.COUNT_PACK_EVENTS;
import static ru.beorg.processing.events.controller.RequestPath.PackEvent.GET_PACKS;
import static ru.beorg.processing.events.controller.RequestPath.PackEvent.GET_PACK_STATS;

@Slf4j
@RestController
public class PackEventController {

    @Autowired PackEventRepository packEventRepository;
    @Autowired PackStatsAggregationService statsAggregationService;
    @Autowired ResponseConverter responseConverter;

    @RequestMapping(GET_PACKS)
    public ResponseEntity<List<PackEventResponse>> getDocuments(@RequestBody PackSearchParameter searchParameters) {
        log.info("Incoming search request with parameters {}", searchParameters);

        List<PackEventResponse> documentEvents = responseConverter.toPackEventResponseList(
                packEventRepository.findAll(searchParameters)
        );

        log.info("Found {} results", documentEvents.size());

        return new ResponseEntity<>(documentEvents, OK);
    }

    @RequestMapping(GET_PACK_STATS)
    public ResponseEntity getStats(@RequestBody PackSearchParameter searchParameters) {
        Iterable<DBObject> stats = statsAggregationService.getStats(searchParameters);

        log.info("Found: {}", stats);

        if (statsAggregationService.containsGroupByClauses(searchParameters)) {
            return new ResponseEntity<>(responseConverter.toStatsResponseList(stats, searchParameters), OK);
        } else {
            return new ResponseEntity<>(responseConverter.transformDateToSeconds(stats), OK);
        }
    }

    @RequestMapping(COUNT_PACK_EVENTS)
    public ResponseEntity<Integer> countDocumentEvents(@RequestBody PackSearchParameter searchParameters) {
        return new ResponseEntity<>(statsAggregationService.countEvents(searchParameters), OK);
    }

    @RequestMapping(value = GET_PACK_STATS, method = DELETE)
    public ResponseEntity<Integer> deleteDocuments(@RequestBody PackSearchParameter searchParameters) {
        log.info("Incoming delete request with parameters {}", searchParameters);

        int deleted = packEventRepository.delete(searchParameters);

        log.info("Deleted {} documents", deleted);

        return new ResponseEntity<>(deleted, OK);
    }

}
