package ru.beorg.processing.events.controller;

import com.mongodb.DBObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.beorg.processing.events.controller.response.ResponseConverter;
import ru.beorg.processing.events.controller.response.entity.UserEventResponse;
import ru.beorg.processing.events.db.repository.event.UserEventRepository;
import ru.beorg.processing.events.db.search.user.UserSearchParameter;
import ru.beorg.processing.events.db.search.user.UserStatsAggregationService;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static ru.beorg.processing.events.controller.RequestPath.UserEvent.COUNT_USER_EVENTS;
import static ru.beorg.processing.events.controller.RequestPath.UserEvent.GET_USERS;
import static ru.beorg.processing.events.controller.RequestPath.UserEvent.GET_USER_STATS;

@Slf4j
@RestController
public class UserEventController {

    @Autowired UserEventRepository userEventRepository;
    @Autowired UserStatsAggregationService statsAggregationService;
    @Autowired ResponseConverter responseConverter;

    @RequestMapping(GET_USERS)
    public ResponseEntity<List<UserEventResponse>> getDocuments(@RequestBody UserSearchParameter searchParameters) {
        log.info("Incoming search request with parameters {}", searchParameters);

        List<UserEventResponse> documentEvents = responseConverter.toUserEventResponseList(
                userEventRepository.findAll(searchParameters)
        );

        log.info("Found {} results", documentEvents.size());

        return new ResponseEntity<>(documentEvents, OK);
    }

    @RequestMapping(GET_USER_STATS)
    public ResponseEntity getStats(@RequestBody UserSearchParameter searchParameters) {
        Iterable<DBObject> stats = statsAggregationService.getStats(searchParameters);

        log.info("Found: {}", stats);

        if (statsAggregationService.containsGroupByClauses(searchParameters)) {
            return new ResponseEntity<>(responseConverter.toStatsResponseList(stats, searchParameters), OK);
        } else {
            return new ResponseEntity<>(responseConverter.transformDateToSeconds(stats), OK);
        }
    }

    @RequestMapping(COUNT_USER_EVENTS)
    public ResponseEntity<Integer> countDocumentEvents(@RequestBody UserSearchParameter searchParameters) {
        return new ResponseEntity<>(statsAggregationService.countEvents(searchParameters), OK);
    }

    @RequestMapping(value = GET_USERS, method = DELETE)
    public ResponseEntity<Integer> deleteDocuments(@RequestBody UserSearchParameter searchParameters) {
        log.info("Incoming delete request with parameters {}", searchParameters);

        int deleted = userEventRepository.delete(searchParameters);

        log.info("Deleted {} documents", deleted);

        return new ResponseEntity<>(deleted, OK);
    }

}
