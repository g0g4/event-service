package ru.beorg.processing.events.controller.response;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.beorg.processing.events.controller.response.entity.*;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.converter.RedisKey;
import ru.beorg.processing.events.db.model.event.DocumentEvent;
import ru.beorg.processing.events.db.model.event.FieldEvent;
import ru.beorg.processing.events.db.model.event.PackEvent;
import ru.beorg.processing.events.db.model.event.UserEvent;
import ru.beorg.processing.events.db.search.RedisGroupableSearchParameter;
import ru.beorg.processing.events.db.search.pack.PackSearchParameter;
import ru.beorg.processing.events.db.search.user.UserSearchParameter;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.time.LocalDate.now;
import static java.time.LocalDate.parse;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;
import static ru.beorg.processing.events.controller.response.entity.StatsGroupFieldName.*;

@Component
public class ResponseConverter {

    @Autowired DateConverter dateConverter;

    public List<DocumentEventResponse> toDocumentEventResponseList(List<DocumentEvent> events) {
        return events.stream().map(this::convertToResponse).collect(toList());
    }

    public List<FieldEventResponse> toFieldEventResponseList(List<FieldEvent> events) {
        return events.stream().map(this::convertToResponse).collect(toList());
    }

    public List<PackEventResponse> toPackEventResponseList(List<PackEvent> events) {
        return events.stream().map(this::convertToResponse).collect(toList());
    }

    public List<UserEventResponse> toUserEventResponseList(List<UserEvent> events) {
        return events.stream().map(this::convertToResponse).collect(toList());
    }

    public List<StatsResponse> toStatsResponseList(Iterable<DBObject> stats, RedisGroupableSearchParameter searchParameters) {
        return stream(stats.spliterator(), false).map(e -> this.convertToStatsResponse(e, searchParameters))
                .collect(toList());
    }

    public List<StatsResponse> toStatsResponseList(Iterable<DBObject> stats, PackSearchParameter searchParameters) {
        return stream(stats.spliterator(), false).map(e -> this.convertToStatsResponse(e, searchParameters))
                .collect(toList());
    }

    public List<StatsResponse> toStatsResponseList(Iterable<DBObject> stats, UserSearchParameter searchParameters) {
        return stream(stats.spliterator(), false).map(e -> this.convertToStatsResponse(e, searchParameters))
                .collect(toList());
    }

    public Iterable<DBObject> transformDateToSeconds(Iterable<DBObject> stats) {
        stats.forEach(e -> e.put(DATE, dateConverter.toEpochSeconds(((Date) e.get(DATE)))));

        return stats;
    }

    public DocumentEventResponse convertToResponse(DocumentEvent documentEvent) {
        return new DocumentEventResponse().setClient(documentEvent.getClient())
                .setCampaign(documentEvent.getCampaign())
                .setDocument(documentEvent.getDocument())
                .setShop(documentEvent.getShop())
                .setEvent(documentEvent.getEvent())
                .setUser(documentEvent.getUser())
                .setComment(documentEvent.getComment())
                .setDate(dateConverter.toEpochSeconds(documentEvent.getDate()));
    }

    public FieldEventResponse convertToResponse(FieldEvent fieldEvent) {
        return new FieldEventResponse().setClient(fieldEvent.getClient())
                .setCampaign(fieldEvent.getCampaign())
                .setDocument(fieldEvent.getDocument())
                .setName(fieldEvent.getName())
                .setEvent(fieldEvent.getEvent())
                .setUser(fieldEvent.getUser())
                .setComment(fieldEvent.getComment())
                .setDate(dateConverter.toEpochSeconds(fieldEvent.getDate()));
    }

    public PackEventResponse convertToResponse(PackEvent packEvent) {
        return new PackEventResponse().setClient(packEvent.getClient())
                .setCampaign(packEvent.getCampaign())
                .setEvent(packEvent.getEvent())
                .setComment(packEvent.getComment())
                .setPack(packEvent.getPack())
                .setShop(packEvent.getShop())
                .setUser(packEvent.getUser())
                .setDate(dateConverter.toEpochSeconds(packEvent.getDate()));
    }

    public UserEventResponse convertToResponse(UserEvent userEvent) {
        return new UserEventResponse().setEvent(userEvent.getEvent())
                .setUser(userEvent.getUser())
                .setInitiator(userEvent.getInitiator())
                .setDate(dateConverter.toEpochSeconds(userEvent.getDate()));
    }

    public StatsResponse convertToStatsResponse(DBObject aggregationResults, RedisGroupableSearchParameter searchParameter) {
        StatsResponse statsResponse = new StatsResponse();
        Map<String, Object> group = new HashMap<>();
        BasicDBObject dbObject = (BasicDBObject) aggregationResults.get("_id");

        if (searchParameter.getByClient() != null && searchParameter.getByClient()) {
            group.put(CLIENT, dbObject.get("client"));
        }

        if (searchParameter.getByCampaign() != null && searchParameter.getByCampaign()) {
            group.put(CAMPAIGN, dbObject.get("campaign"));
        }

        if (searchParameter.getByEvent() != null && searchParameter.getByEvent()) {
            group.put(EVENT, dbObject.get("event"));
        }

        if (searchParameter.getByDate() != null && searchParameter.getByDate()) {
            group.put(DATE, dateConverter.toEpochSeconds(parse(dbObject.get("date").toString()).atStartOfDay()));
        }

        statsResponse.setCount(((Number) aggregationResults.get("count")).longValue());
        statsResponse.setGroup(group);

        return statsResponse;
    }

    public StatsResponse convertToStatsResponse(DBObject aggregationResults, PackSearchParameter searchParameter) {
        StatsResponse statsResponse = new StatsResponse();
        Map<String, Object> group = new HashMap<>();
        BasicDBObject dbObject = (BasicDBObject) aggregationResults.get("_id");

        if (searchParameter.getByDate() != null && searchParameter.getByDate()) {
            group.put(DATE, dateConverter.toEpochSeconds(parse(dbObject.get("date").toString()).atStartOfDay()));
        }

        if (searchParameter.getByEvent() != null && searchParameter.getByEvent()) {
            group.put(EVENT, dbObject.get("event"));
        }

        if (searchParameter.getByClient() != null && searchParameter.getByClient()) {
            group.put(CLIENT, dbObject.get("client"));
        }

        if (searchParameter.getByCampaign() != null && searchParameter.getByCampaign()) {
            group.put(CAMPAIGN, dbObject.get("campaign"));
        }

        if (searchParameter.getByShop() != null && searchParameter.getByShop()) {
            group.put(SHOP, dbObject.get("shop"));
        }

        if (searchParameter.getByPack() != null && searchParameter.getByPack()) {
            group.put(PACK, dbObject.get("pack"));
        }

        if (searchParameter.getByUser() != null && searchParameter.getByUser()) {
            group.put(USER, dbObject.get("user"));
        }

        statsResponse.setCount(((Number) aggregationResults.get("count")).longValue());
        statsResponse.setGroup(group);

        return statsResponse;
    }

    public StatsResponse convertToStatsResponse(DBObject aggregationResults, UserSearchParameter searchParameter) {
        StatsResponse statsResponse = new StatsResponse();
        Map<String, Object> group = new HashMap<>();
        BasicDBObject dbObject = (BasicDBObject) aggregationResults.get("_id");

        if (searchParameter.getByEvent() != null && searchParameter.getByEvent()) {
            group.put(EVENT, dbObject.get("event"));
        }

        if (searchParameter.getByUser() != null && searchParameter.getByUser()) {
            group.put(USER, dbObject.get("user"));
        }

        if (searchParameter.getByInitiator() != null && searchParameter.getByInitiator()) {
            group.put(INITIATOR, dbObject.get("initiator"));
        }

        if (searchParameter.getByDate() != null && searchParameter.getByDate()) {
            group.put(DATE, dateConverter.toEpochSeconds(parse(dbObject.get("date").toString()).atStartOfDay()));
        }

        statsResponse.setCount(((Number) aggregationResults.get("count")).longValue());
        statsResponse.setGroup(group);

        return statsResponse;
    }

    public StatsResponse convertToStatsResponse(RedisKey redisKey, Long count,
                                                RedisGroupableSearchParameter searchParameters) {
        StatsResponse statsResponse = new StatsResponse();
        Map<String, Object> group = new HashMap<>();

        if (searchParameters.getByDate() != null && searchParameters.getByDate()) {
            group.put(DATE, dateConverter.toEpochSeconds(now().atStartOfDay()));
        }

        if (redisKey.getEvent() != null) {
            group.put(EVENT, redisKey.getEvent());
        }

        if (redisKey.getClient() != null) {
            group.put(CLIENT, redisKey.getClient());
        }

        if (redisKey.getCampaign() != null) {
            group.put(CAMPAIGN, redisKey.getCampaign());
        }

        statsResponse.setCount(count);
        statsResponse.setGroup(group);

        return statsResponse;
    }

}
