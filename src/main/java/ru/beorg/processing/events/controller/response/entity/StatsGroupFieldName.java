package ru.beorg.processing.events.controller.response.entity;

public interface StatsGroupFieldName {

    String DATE = "date";
    String EVENT = "event";
    String CLIENT = "client";
    String CAMPAIGN = "campaign";
    String SHOP = "shop";
    String USER = "user";
    String NAME = "name";
    String PACK = "pack";
    String INITIATOR = "initiator";

}
