package ru.beorg.processing.events.controller.response.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class FieldEventResponse {

    private Integer event;
    private Integer client;
    private String campaign;
    private String comment;
    private Integer document;
    private String name;
    private Long date;
    private String user;

}
