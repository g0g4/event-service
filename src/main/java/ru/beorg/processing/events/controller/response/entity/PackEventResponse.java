package ru.beorg.processing.events.controller.response.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PackEventResponse {

    private Integer event;
    private Integer client;
    private String campaign;
    private String comment;
    private Integer pack;
    private Integer shop;
    private String user;
    private Long date;

}
