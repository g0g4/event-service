package ru.beorg.processing.events.listeners;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.model.event.FieldEvent;
import ru.beorg.processing.events.db.repository.event.FieldEventRepository;
import ru.beorg.processing.events.increment.RedisIncrementService;
import ru.beorg.processing.rabbit.common.message.event.FieldEventMessage;

import static ru.beorg.processing.rabbit.common.QueueNames.EventQueues.FIELD_EVENT;

@Slf4j
@Component
public class FieldEventListener {
    @Autowired RabbitTemplate rabbitTemplate;
    @Autowired FieldEventRepository repository;
    @Autowired RedisIncrementService redisIncrementService;
    @Autowired DateConverter dateConverter;

    @RabbitListener(queues = FIELD_EVENT)
    public void saveFieldEvent(@NonNull FieldEventMessage message) {
        log.info("Received message to increment field event: {}", message);

        if (!isCorrectMessage(message)) {
            log.warn("Received incorrect message for saving: {}", message);
            return;
        }

        FieldEvent document = new FieldEvent()
                .setClient(message.getClient())
                .setCampaign(message.getCampaign())
                .setDocument(message.getDocument())
                .setName(message.getName())
                .setEvent(message.getEvent())
                .setUser(message.getUser())
                .setComment(message.getComment())
                .setDate(dateConverter.convertDate(message.getDate()));

        FieldEvent fieldEvent = repository.save(document);
        log.info("Field event: {} was inserted", fieldEvent);

        redisIncrementService.increment(fieldEvent);
    }

    boolean isCorrectMessage(FieldEventMessage message) {
        return message.getClient() != null && message.getCampaign() != null
                && message.getDocument() != null && message.getName() != null
                && message.getEvent() != null;
    }
}
