package ru.beorg.processing.events.listeners;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.model.event.DocumentEvent;
import ru.beorg.processing.events.db.repository.event.DocumentEventRepository;
import ru.beorg.processing.events.increment.RedisIncrementService;
import ru.beorg.processing.rabbit.common.message.event.DocumentEventMessage;

import static ru.beorg.processing.rabbit.common.QueueNames.EventQueues.DOCUMENT_EVENT;

@Slf4j
@Component
public class DocumentEventListener {
    @Autowired RabbitTemplate rabbitTemplate;
    @Autowired DocumentEventRepository repository;
    @Autowired RedisIncrementService redisIncrementService;
    @Autowired DateConverter dateConverter;

    @RabbitListener(queues = DOCUMENT_EVENT)
    public void saveDocumentEvent(@NonNull DocumentEventMessage message) {
        log.info("Received message to increment document event: {}", message);

        if (!isCorrectMessage(message)) {
            log.warn("Received incorrect message for saving: {}", message);
            return;
        }

        DocumentEvent document = new DocumentEvent()
                .setClient(message.getClient())
                .setCampaign(message.getCampaign())
                .setShop(message.getShop())
                .setDocument(message.getDocument())
                .setUser(message.getUser())
                .setEvent(message.getEvent())
                .setComment(message.getComment())
                .setDate(dateConverter.convertDate(message.getDate()));

        DocumentEvent documentEvent = repository.save(document);
        log.info("Document event: {} was inserted", documentEvent);

        redisIncrementService.increment(documentEvent);
    }

    boolean isCorrectMessage(DocumentEventMessage message) {
        return message.getClient() != null && message.getCampaign() != null
                && message.getDocument() != null && message.getEvent() != null;
    }
}