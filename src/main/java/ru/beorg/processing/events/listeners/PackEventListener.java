package ru.beorg.processing.events.listeners;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.model.event.PackEvent;
import ru.beorg.processing.events.db.repository.event.PackEventRepository;
import ru.beorg.processing.rabbit.common.message.event.PackEventMessage;

import static ru.beorg.processing.rabbit.common.QueueNames.EventQueues.PACK_EVENT;

@Slf4j
@Component
public class PackEventListener {
    @Autowired RabbitTemplate rabbitTemplate;
    @Autowired PackEventRepository repository;
    @Autowired DateConverter dateConverter;

    @RabbitListener(queues = PACK_EVENT)
    public void savePackEvent(@NonNull PackEventMessage message) {
        log.info("Received message to increment pack event: {}", message);

        if (!isCorrectMessage(message)) {
            log.warn("Received incorrect message for saving: {}", message);
            return;
        }

        PackEvent document = new PackEvent()
                .setClient(message.getClient())
                .setCampaign(message.getCampaign())
                .setShop(message.getShop())
                .setPack(message.getPack())
                .setEvent(message.getEvent())
                .setUser(message.getUser())
                .setComment(message.getComment())
                .setDate(dateConverter.convertDate(message.getDate()));

        PackEvent packEvent = repository.save(document);
        log.info("Pack event: {} was inserted", packEvent);
    }

    boolean isCorrectMessage(PackEventMessage message) {
        return message.getClient() != null && message.getCampaign() != null
                 && message.getPack() != null && message.getEvent() != null;
    }
}
