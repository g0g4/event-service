package ru.beorg.processing.events.listeners;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.model.event.UserEvent;
import ru.beorg.processing.events.db.repository.event.UserEventRepository;
import ru.beorg.processing.rabbit.common.message.event.UserEventMessage;

import static ru.beorg.processing.rabbit.common.QueueNames.EventQueues.USER_EVENT;

@Slf4j
@Component
public class UserEventListener {
    @Autowired RabbitTemplate rabbitTemplate;
    @Autowired UserEventRepository repository;
    @Autowired DateConverter dateConverter;

    @RabbitListener(queues = USER_EVENT)
    public void saveUserEvent(@NonNull UserEventMessage message) {
        log.info("Received message to increment user event: {}", message);

        if (!isCorrectMessage(message)) {
            log.warn("Received incorrect message for saving: {}", message);
            return;
        }

        UserEvent document = new UserEvent()
                .setUser(message.getUser())
                .setInitiator(message.getInitiator())
                .setEvent(message.getEvent())
                .setDate(dateConverter.convertDate(message.getDate()));

        UserEvent userEvent = repository.save(document);
        log.info("User event: {} was inserted", userEvent);
    }

    boolean isCorrectMessage(UserEventMessage message) {
        return message.getUser() != null && message.getEvent() != null;
    }
}
