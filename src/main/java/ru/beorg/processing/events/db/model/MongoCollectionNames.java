package ru.beorg.processing.events.db.model;

public interface MongoCollectionNames {

    String DOCUMENT_AGGREGATION = "document_daily_aggregation";
    String FIELD_AGGREGATION = "field_daily_aggregation";

    String DOCUMENT_EVENT = "documents";
    String FIELD_EVENT = "fields";
    String PACK_EVENT = "packs";
    String USER_EVENT = "users";

}
