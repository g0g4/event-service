package ru.beorg.processing.events.db.search;

import lombok.Data;

import java.util.List;

@Data
public abstract class RedisGroupableSearchParameter {

    protected List<Integer> event;
    protected List<Integer> client;
    protected List<String> campaign;
    protected Integer document;
    protected String comment;
    protected String user;
    protected Long start;
    protected Long end;

    public abstract Boolean getByClient();

    public abstract Boolean getByCampaign();

    public abstract Boolean getByEvent();

    public abstract Boolean getByDate();

}
