package ru.beorg.processing.events.db.search.aggregation;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Iterator;
import java.util.List;

@Slf4j
public abstract class AbstractAggregationService<E> {

    @Autowired protected MongoTemplate mongoTemplate;

    public Iterable<DBObject> getStats(E searchParameters, String collectionName) {
        if (containsGroupByClauses(searchParameters)) {
            return getAggregatedStats(searchParameters, collectionName);
        } else {
            return getUngroupedStats(searchParameters, collectionName);
        }
    }

    protected Iterable<DBObject> getAggregatedStats(E searchParameters, String collectionName) {
        List<BasicDBObject> clauses = buildGroupQueryWithParameters(searchParameters);
        DBCollection dbCollection = mongoTemplate.getCollection(collectionName);

        return dbCollection.aggregate(clauses).results();
    }

    protected Iterable<DBObject> getUngroupedStats(E searchParameters, String collectionName) {
        BasicDBObject query = buildFindQueryWithParameters(searchParameters);
        DBCollection dbCollection = mongoTemplate.getCollection(collectionName);

        return dbCollection.find(query, new BasicDBObject("_id", 0).append("_class", 0)).toArray();
    }

    public int countEvents(E searchParameters, String collectionName) {
        if (containsGroupByClauses(searchParameters)) {
            log.error("Cannot count events with 'group by' clause. All 'by_' fields should be removed from request.");

            throw new IllegalArgumentException("Cannot count events with 'group by' clause. All 'by_' fields should be removed from request.");
        } else {
            List<BasicDBObject> clauses = buildGroupQueryWithParameters(searchParameters);
            DBCollection dbCollection = mongoTemplate.getCollection(collectionName);

            return extractEventCountFromAggregationResults(dbCollection.aggregate(clauses).results());
        }
    }

    private int extractEventCountFromAggregationResults(Iterable<DBObject> results) {
        Iterator<DBObject> resultsIterator = results.iterator();
        if (resultsIterator.hasNext()) {
            BasicDBObject dbObject = (BasicDBObject) resultsIterator.next();

            return ((Number) dbObject.get("count")).intValue();
        } else {
            return 0;
        }
    }

    protected List<BasicDBObject> buildGroupQueryWithParameters(E searchParameters) {
        MongoGroupQueryBuilder queryBuilder = new MongoGroupQueryBuilder();

        addGroupByClauses(queryBuilder, searchParameters);
        addMatchClauses(queryBuilder, searchParameters);
        addUnwindClauses(queryBuilder);
        addProjectFields(queryBuilder);

        return queryBuilder.buildGroupQuery();
    }

    protected BasicDBObject buildFindQueryWithParameters(E searchParameters) {
        MongoGroupQueryBuilder queryBuilder = new MongoGroupQueryBuilder();

        addFindClauses(queryBuilder, searchParameters);

        return queryBuilder.buildFindQuery();
    }


    protected void addUnwindClauses(MongoGroupQueryBuilder queryBuilder) {

    }

    public abstract boolean containsGroupByClauses(E searchParameters);
    protected abstract void addGroupByClauses(MongoGroupQueryBuilder queryBuilder, E searchParameters);
    protected abstract void addMatchClauses(MongoGroupQueryBuilder queryBuilder, E searchParameters);
    protected abstract void addProjectFields(MongoGroupQueryBuilder queryBuilder);
    protected abstract void addFindClauses(MongoGroupQueryBuilder queryBuilder, E searchParameters);

}
