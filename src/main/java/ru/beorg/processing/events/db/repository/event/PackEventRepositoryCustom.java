package ru.beorg.processing.events.db.repository.event;

import ru.beorg.processing.events.db.model.event.PackEvent;
import ru.beorg.processing.events.db.search.pack.PackSearchParameter;

import java.util.List;

public interface PackEventRepositoryCustom {

    List<PackEvent> findAll(PackSearchParameter searchParameters);

    int delete(PackSearchParameter searchParameters);

}
