package ru.beorg.processing.events.db.model.event;

import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

import static ru.beorg.processing.events.db.model.MongoCollectionNames.USER_EVENT;

@Data
@Accessors(chain = true)
@Document(collection = USER_EVENT)
public class UserEvent {

    @Id
    private String id;
    private String user;
    private String initiator;
    private Integer event;
    private LocalDateTime date;
}
