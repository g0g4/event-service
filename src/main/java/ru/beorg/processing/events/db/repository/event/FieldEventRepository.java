package ru.beorg.processing.events.db.repository.event;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.beorg.processing.events.db.model.event.FieldEvent;

public interface FieldEventRepository extends MongoRepository<FieldEvent, String>, FieldEventRepositoryCustom {
}
