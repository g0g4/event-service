package ru.beorg.processing.events.db.search.field;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.beorg.processing.events.db.search.RedisGroupableSearchParameter;

import java.util.List;

@Data
@Accessors(chain = true)
public class FieldSearchParameter extends RedisGroupableSearchParameter {

    private List<String> name;
    private Long date;

    @JsonProperty("by_date")
    private Boolean byDate;

    @JsonProperty("by_event")
    private Boolean byEvent;

    @JsonProperty("by_campaign")
    private Boolean byCampaign;

    @JsonProperty("by_client")
    private Boolean byClient;

}