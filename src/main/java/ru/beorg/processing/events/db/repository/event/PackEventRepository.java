package ru.beorg.processing.events.db.repository.event;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.beorg.processing.events.db.model.event.PackEvent;

public interface PackEventRepository extends MongoRepository<PackEvent, String>, PackEventRepositoryCustom {
}
