package ru.beorg.processing.events.db.repository.event;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.beorg.processing.events.db.model.event.UserEvent;

public interface UserEventRepository extends MongoRepository<UserEvent, String>, UserEventRepositoryCustom {
}
