package ru.beorg.processing.events.db.model.aggregation;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static ru.beorg.processing.events.db.model.MongoCollectionNames.DOCUMENT_AGGREGATION;

@Data
@Accessors(chain = true)
@Document(collection = DOCUMENT_AGGREGATION)
public class DocumentDailyAggregation {

    @Id
    private String id;
    private Integer client;
    private String campaign;
    private Integer shop;
    private LocalDate date;
    private List<EventCount> events;

    public DocumentDailyAggregation() {
        events = new ArrayList<>();
    }
}
