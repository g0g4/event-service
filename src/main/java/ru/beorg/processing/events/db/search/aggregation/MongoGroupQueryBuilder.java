package ru.beorg.processing.events.db.search.aggregation;

import com.mongodb.BasicDBObject;

import java.util.ArrayList;
import java.util.List;

public class MongoGroupQueryBuilder {

    private List<BasicDBObject> clauses;

    private BasicDBObject matchClause;
    private BasicDBObject matchFields;
    private BasicDBObject matchAfterUnwindClause;
    private BasicDBObject matchAfterUnwindFields;
    private BasicDBObject unwindClause;
    private BasicDBObject projectClause;
    private BasicDBObject projectFields;
    private BasicDBObject groupByClause;
    private BasicDBObject groupById;
    private BasicDBObject groupByFields;
    private BasicDBObject findClause;

    public MongoGroupQueryBuilder() {
        init();
    }

    private void init() {
        clauses = new ArrayList<>(2);

        matchClause = new BasicDBObject();
        matchAfterUnwindClause = new BasicDBObject();
        matchFields = new BasicDBObject();
        matchAfterUnwindFields = new BasicDBObject();
        unwindClause = new BasicDBObject();
        projectClause = new BasicDBObject();
        projectFields = new BasicDBObject();
        groupByClause = new BasicDBObject();
        groupById = new BasicDBObject();
        groupByFields = new BasicDBObject();
        findClause = new BasicDBObject();

        matchClause.append("$match", matchFields);
        matchAfterUnwindClause.append("$match", matchAfterUnwindFields);
        projectClause.append("$project", projectFields);
        groupById.append("_id", groupByFields);
        groupByClause.append("$group", groupById);

        clauses.add(matchClause);
        clauses.add(projectClause);
        clauses.add(matchAfterUnwindClause);
        clauses.add(groupByClause);
    }

    public List<BasicDBObject> buildGroupQuery() {
        return clauses;
    }

    public BasicDBObject buildFindQuery() {
        return findClause;
    }

    public void addGroupByCountMethod(String key, Object value) {
        groupById.append("count", new BasicDBObject(key, value));
    }

    public void addGroupByField(String fieldName) {
        groupByFields.append(fieldName, "$" + fieldName);
    }

    public void addUnwindField(String fieldName) {
        unwindClause.append("$unwind", "$" + fieldName);

        if (!clauses.contains(unwindClause)) {
            clauses.add(1, unwindClause);
        }
    }

    public void addProjectField(String fieldName, Object value) {
        projectFields.append(fieldName, value);
    }

    public void addMatchEqualsCondition(String fieldName, Object value) {
        matchFields.append(fieldName, value);
    }

    public void addMatchInCondition(String fieldName, List value) {
        matchFields.append(fieldName, new BasicDBObject("$in", value.toArray()));
    }

    public void addMatchGteCondition(String fieldName, Object value) {
        if (matchFields.get(fieldName) == null) {
            matchFields.append(fieldName, new BasicDBObject("$gte", value));
        } else {
            ((BasicDBObject) matchFields.get(fieldName)).append("$gte", value);
        }
    }

    public void addMatchLteCondition(String fieldName, Object value) {
        if (matchFields.get(fieldName) == null) {
            matchFields.append(fieldName, new BasicDBObject("$lte", value));
        } else {
            ((BasicDBObject) matchFields.get(fieldName)).append("$lte", value);
        }
    }

    public void addMatchAfterUnwindInCondition(String fieldName, List value) {
        matchAfterUnwindFields.append(fieldName, new BasicDBObject("$in", value.toArray()));
    }

    public void addFindEqCondition(String fieldName, Object value) {
        findClause.append(fieldName, new BasicDBObject("$eq", value));
    }

    public void addFindInCondition(String fieldName, List value) {
        findClause.append(fieldName, new BasicDBObject("$in", value.toArray()));
    }

    public void addFindGteCondition(String fieldName, Object value) {
        if (findClause.get(fieldName) == null) {
            findClause.append(fieldName, new BasicDBObject("$gte", value));
        } else {
            ((BasicDBObject) findClause.get(fieldName)).append("$gte", value);
        }
    }

    public void addFindLteCondition(String fieldName, Object value) {
        if (findClause.get(fieldName) == null) {
            findClause.append(fieldName, new BasicDBObject("$lte", value));
        } else {
            ((BasicDBObject) findClause.get(fieldName)).append("$lte", value);
        }
    }

}
