package ru.beorg.processing.events.db.model.event;

import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

import static ru.beorg.processing.events.db.model.MongoCollectionNames.DOCUMENT_EVENT;

@Data
@Accessors(chain = true)
@Document(collection = DOCUMENT_EVENT)
public class DocumentEvent {

    @Id
    private String id;
    private Integer client;
    private String campaign;
    private Integer shop;
    private Integer document;
    private String user;
    private Integer event;
    private String comment;
    private LocalDateTime date;
}
