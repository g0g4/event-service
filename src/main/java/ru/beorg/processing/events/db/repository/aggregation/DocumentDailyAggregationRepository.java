package ru.beorg.processing.events.db.repository.aggregation;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.beorg.processing.events.db.model.aggregation.DocumentDailyAggregation;

public interface DocumentDailyAggregationRepository extends MongoRepository<DocumentDailyAggregation, String> {
}
