package ru.beorg.processing.events.db.search.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import ru.beorg.processing.events.db.model.event.UserEvent;
import ru.beorg.processing.events.db.repository.event.UserEventRepositoryCustom;

import java.util.List;

public class UserEventRepositoryImpl implements UserEventRepositoryCustom {

    @Autowired MongoTemplate mongoTemplate;
    @Autowired UserCriteriaBuilder userCriteriaBuilder;

    @Override
    public List<UserEvent> findAll(UserSearchParameter searchParameters) {
        Criteria criteria = userCriteriaBuilder.build(searchParameters);

        return mongoTemplate.find(Query.query(criteria), UserEvent.class);
    }

    public int delete(UserSearchParameter searchParameters) {
        Criteria criteria = userCriteriaBuilder.build(searchParameters);

        return mongoTemplate.remove(Query.query(criteria), UserEvent.class).getN();
    }

}
