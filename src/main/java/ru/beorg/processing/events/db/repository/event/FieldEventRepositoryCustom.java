package ru.beorg.processing.events.db.repository.event;

import ru.beorg.processing.events.db.model.event.FieldEvent;
import ru.beorg.processing.events.db.search.field.FieldSearchParameter;

import java.util.List;

public interface FieldEventRepositoryCustom {

    List<FieldEvent> findAll(FieldSearchParameter searchParameters);

    int delete(FieldSearchParameter searchParameters);

}
