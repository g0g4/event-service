package ru.beorg.processing.events.db.repository.event;

import ru.beorg.processing.events.db.model.event.UserEvent;
import ru.beorg.processing.events.db.search.user.UserSearchParameter;

import java.util.List;

public interface UserEventRepositoryCustom {

    List<UserEvent> findAll(UserSearchParameter searchParameters);

    int delete(UserSearchParameter searchParameters);

}
