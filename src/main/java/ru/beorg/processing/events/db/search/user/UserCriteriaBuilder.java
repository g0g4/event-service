package ru.beorg.processing.events.db.search.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;
import ru.beorg.processing.events.converter.DateConverter;

import java.time.LocalDateTime;

import static ru.beorg.processing.events.db.search.SearchParameterNames.CommonFields.*;
import static ru.beorg.processing.events.db.search.SearchParameterNames.DocumentFieldPackEventFields.UserEventFields.INITIATOR;

@Component
public class UserCriteriaBuilder {

    @Autowired DateConverter dateConverter;

    public Criteria build(UserSearchParameter searchParameters) {
        Criteria criteria = new Criteria();

        addEventCriteria(criteria, searchParameters);
        addUserCriteria(criteria, searchParameters);
        addInitiatorCriteria(criteria, searchParameters);
        addDateCriteria(criteria, searchParameters);

        return criteria;
    }

    public Criteria addEventCriteria(Criteria criteria, UserSearchParameter searchParameters) {
        if (searchParameters.getEvent() != null) {
            criteria.and(EVENT).in(searchParameters.getEvent());
        }

        return criteria;
    }

    public Criteria addUserCriteria(Criteria criteria, UserSearchParameter searchParameters) {
        if (searchParameters.getUser() != null) {
            criteria.and(USER).is(searchParameters.getUser());
        }

        return criteria;
    }

    public Criteria addInitiatorCriteria(Criteria criteria, UserSearchParameter searchParameters) {
        if (searchParameters.getInitiator() != null) {
            criteria.and(INITIATOR).is(searchParameters.getInitiator());
        }

        return criteria;
    }

    public Criteria addDateCriteria(Criteria criteria, UserSearchParameter searchParameters) {
        if (searchParameters.getDate() != null) {
            LocalDateTime date = dateConverter.convertDate(searchParameters.getDate()).toLocalDate().atStartOfDay();
            criteria.and(DATE).gte(date).lte(date.plusDays(1));
        }

        return criteria;
    }

}
