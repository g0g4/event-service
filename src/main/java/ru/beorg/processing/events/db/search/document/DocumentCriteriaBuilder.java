package ru.beorg.processing.events.db.search.document;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;
import ru.beorg.processing.events.converter.DateConverter;

import static ru.beorg.processing.events.db.search.SearchParameterNames.CommonFields.DATE;
import static ru.beorg.processing.events.db.search.SearchParameterNames.CommonFields.EVENT;
import static ru.beorg.processing.events.db.search.SearchParameterNames.DocumentFieldPackEventFields.*;
import static ru.beorg.processing.events.db.search.SearchParameterNames.DocumentFieldPackEventFields.DocumentEventFields.DOCUMENT_ID;
import static ru.beorg.processing.events.db.search.SearchParameterNames.DocumentFieldPackEventFields.DocumentEventFields.SHOP_ID;

@Component
public class DocumentCriteriaBuilder {

    @Autowired DateConverter dateConverter;

    public Criteria build(DocumentSearchParameter searchParameters) {
        Criteria criteria = new Criteria();

        addClientCriteria(criteria, searchParameters);
        addCampaignCriteria(criteria, searchParameters);
        addDocumentCriteria(criteria, searchParameters);
        addDateRangeCriteria(criteria, searchParameters);
        addShopCriteria(criteria, searchParameters);
        addEventCriteria(criteria, searchParameters);
        addCommentCriteria(criteria, searchParameters);

        return criteria;
    }

    public Criteria addClientCriteria(Criteria criteria, DocumentSearchParameter searchParameters) {
        if (searchParameters.getClient() != null) {
            criteria.and(CLIENT_ID).in(searchParameters.getClient());
        }

        return criteria;
    }

    public Criteria addCampaignCriteria(Criteria criteria, DocumentSearchParameter searchParameters) {
        if (searchParameters.getCampaign() != null) {
            criteria.and(CAMPAIGN_ID).in(searchParameters.getCampaign());
        }

        return criteria;
    }

    public Criteria addDocumentCriteria(Criteria criteria, DocumentSearchParameter searchParameters) {
        if (searchParameters.getDocument() != null) {
            criteria.and(DOCUMENT_ID).is(searchParameters.getDocument());
        }

        return criteria;
    }

    public Criteria addShopCriteria(Criteria criteria, DocumentSearchParameter searchParameters) {
        if (searchParameters.getShop() != null) {
            criteria.and(SHOP_ID).in(searchParameters.getShop());
        }

        return criteria;
    }

    public Criteria addEventCriteria(Criteria criteria, DocumentSearchParameter searchParameters) {
        if (searchParameters.getEvent() != null) {
            criteria.and(EVENT).in(searchParameters.getEvent());
        }

        return criteria;
    }

    public Criteria addCommentCriteria(Criteria criteria, DocumentSearchParameter searchParameters) {
        if (searchParameters.getComment() != null) {
            criteria.and(COMMENT).is(searchParameters.getComment());
        }

        return criteria;
    }

    public Criteria addDateRangeCriteria(Criteria criteria, DocumentSearchParameter searchParameters) {
        Criteria dateCriteria = Criteria.where(DATE);
        boolean isParameterPresent = false;

        if (searchParameters.getStart() != null) {
            dateCriteria.gte(dateConverter.convertDate(searchParameters.getStart()));
            isParameterPresent = true;
        }

        if (searchParameters.getEnd() != null) {
            dateCriteria.lte(dateConverter.convertDate(searchParameters.getEnd()));
            isParameterPresent = true;
        }

        if (isParameterPresent) {
            criteria.andOperator(dateCriteria);
        }

        return criteria;
    }

}
