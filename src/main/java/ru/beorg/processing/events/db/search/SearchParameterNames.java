package ru.beorg.processing.events.db.search;

public interface SearchParameterNames {

    interface CommonFields {
        String USER = "user";
        String EVENT = "event";
        String DATE = "date";
    }

    interface DocumentFieldPackEventFields {
        String CLIENT_ID = "client";
        String CAMPAIGN_ID = "campaign";
        String COMMENT = "comment";

        interface DocumentEventFields {
            String DOCUMENT_ID = "document";
            String SHOP_ID = "shop";
        }

        interface FieldEventFields {
            String DOCUMENT_ID = "document";
            String NAME = "name";
        }

        interface PackEventFields {
            String SHOP_ID = "shop";
            String PACK_ID = "pack";
        }

        interface UserEventFields {
            String INITIATOR = "initiator";
        }
    }

}