package ru.beorg.processing.events.db.search.field;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import ru.beorg.processing.events.db.model.event.FieldEvent;
import ru.beorg.processing.events.db.repository.event.FieldEventRepositoryCustom;

import java.util.List;

public class FieldEventRepositoryImpl implements FieldEventRepositoryCustom {

    @Autowired MongoTemplate mongoTemplate;
    @Autowired FieldCriteriaBuilder fieldCriteriaBuilder;

    @Override
    public List<FieldEvent> findAll(FieldSearchParameter searchParameters) {
        Criteria criteria = fieldCriteriaBuilder.build(searchParameters);

        return mongoTemplate.find(Query.query(criteria), FieldEvent.class);
    }

    public int delete(FieldSearchParameter searchParameters) {
        Criteria criteria = fieldCriteriaBuilder.build(searchParameters);

        return mongoTemplate.remove(Query.query(criteria), FieldEvent.class).getN();
    }

}
