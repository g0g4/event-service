package ru.beorg.processing.events.db.search.document;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import ru.beorg.processing.events.db.model.event.DocumentEvent;
import ru.beorg.processing.events.db.repository.event.DocumentEventRepositoryCustom;

import java.util.List;

public class DocumentEventRepositoryImpl implements DocumentEventRepositoryCustom {

    @Autowired MongoTemplate mongoTemplate;
    @Autowired DocumentCriteriaBuilder documentCriteriaBuilder;

    @Override
    public List<DocumentEvent> findAll(DocumentSearchParameter searchParameters) {
        Criteria criteria = documentCriteriaBuilder.build(searchParameters);

        return mongoTemplate.find(Query.query(criteria), DocumentEvent.class);
    }

    public int delete(DocumentSearchParameter searchParameters) {
        Criteria criteria = documentCriteriaBuilder.build(searchParameters);

        return mongoTemplate.remove(Query.query(criteria), DocumentEvent.class).getN();
    }

}
