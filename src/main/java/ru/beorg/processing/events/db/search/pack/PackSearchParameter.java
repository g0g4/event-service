package ru.beorg.processing.events.db.search.pack;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class PackSearchParameter {

    private List<Integer> event;
    private List<Integer> client;
    private List<String> campaign;
    private List<Integer> shop;
    private List<Integer> pack;
    private String user;
    private String comment;
    private Long date;
    private Long start;
    private Long end;

    @JsonProperty("by_date")
    private Boolean byDate;

    @JsonProperty("by_event")
    private Boolean byEvent;

    @JsonProperty("by_campaign")
    private Boolean byCampaign;

    @JsonProperty("by_client")
    private Boolean byClient;

    @JsonProperty("by_shop")
    private Boolean byShop;

    @JsonProperty("by_pack")
    private Boolean byPack;

    @JsonProperty("by_user")
    private Boolean byUser;

}
