package ru.beorg.processing.events.db.search.pack;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;
import ru.beorg.processing.events.converter.DateConverter;

import java.time.LocalDateTime;

import static ru.beorg.processing.events.db.search.SearchParameterNames.CommonFields.*;
import static ru.beorg.processing.events.db.search.SearchParameterNames.DocumentFieldPackEventFields.*;
import static ru.beorg.processing.events.db.search.SearchParameterNames.DocumentFieldPackEventFields.PackEventFields.PACK_ID;
import static ru.beorg.processing.events.db.search.SearchParameterNames.DocumentFieldPackEventFields.PackEventFields.SHOP_ID;

@Component
public class PackCriteriaBuilder {

    @Autowired DateConverter dateConverter;

    public Criteria build(PackSearchParameter searchParameters) {
        Criteria criteria = new Criteria();

        addEventCriteria(criteria, searchParameters);
        addClientCriteria(criteria, searchParameters);
        addCampaignCriteria(criteria, searchParameters);
        addShopCriteria(criteria, searchParameters);
        addPackCriteria(criteria, searchParameters);
        addUserCriteria(criteria, searchParameters);
        addCommentCriteria(criteria, searchParameters);

        if (searchParameters.getDate() != null) {
            addDateCriteria(criteria, searchParameters);
        } else if (searchParameters.getStart() != null || searchParameters.getEnd() != null) {
            addDateRangeCriteria(criteria, searchParameters);
        }

        return criteria;
    }

    public Criteria addClientCriteria(Criteria criteria, PackSearchParameter searchParameters) {
        if (searchParameters.getClient() != null) {
            criteria.and(CLIENT_ID).in(searchParameters.getClient());
        }

        return criteria;
    }

    public Criteria addCampaignCriteria(Criteria criteria, PackSearchParameter searchParameters) {
        if (searchParameters.getCampaign() != null) {
            criteria.and(CAMPAIGN_ID).in(searchParameters.getCampaign());
        }

        return criteria;
    }

    public Criteria addShopCriteria(Criteria criteria, PackSearchParameter searchParameters) {
        if (searchParameters.getShop() != null) {
            criteria.and(SHOP_ID).in(searchParameters.getShop());
        }

        return criteria;
    }

    public Criteria addUserCriteria(Criteria criteria, PackSearchParameter searchParameters) {
        if (searchParameters.getUser() != null) {
            criteria.and(USER).is(searchParameters.getUser());
        }

        return criteria;
    }

    public Criteria addEventCriteria(Criteria criteria, PackSearchParameter searchParameters) {
        if (searchParameters.getEvent() != null) {
            criteria.and(EVENT).in(searchParameters.getEvent());
        }

        return criteria;
    }

    public Criteria addCommentCriteria(Criteria criteria, PackSearchParameter searchParameters) {
        if (searchParameters.getComment() != null) {
            criteria.and(COMMENT).is(searchParameters.getComment());
        }

        return criteria;
    }

    public Criteria addDateCriteria(Criteria criteria, PackSearchParameter searchParameters) {
        if (searchParameters.getDate() != null) {
            LocalDateTime date = dateConverter.convertDate(searchParameters.getDate()).toLocalDate().atStartOfDay();
            criteria.and(DATE).gte(date).lte(date.plusDays(1));
        }

        return criteria;
    }

    public Criteria addPackCriteria(Criteria criteria, PackSearchParameter searchParameters) {
        if (searchParameters.getPack() != null) {
            criteria.and(PACK_ID).in(searchParameters.getPack());
        }

        return criteria;
    }

    public Criteria addDateRangeCriteria(Criteria criteria, PackSearchParameter searchParameters) {
        Criteria dateCriteria = Criteria.where(DATE);
        boolean isParameterPresent = false;

        if (searchParameters.getStart() != null) {
            dateCriteria.gte(dateConverter.convertDate(searchParameters.getStart()));
            isParameterPresent = true;
        }

        if (searchParameters.getEnd() != null) {
            dateCriteria.lte(dateConverter.convertDate(searchParameters.getEnd()));
            isParameterPresent = true;
        }

        if (isParameterPresent) {
            criteria.andOperator(dateCriteria);
        }

        return criteria;
    }

}
