package ru.beorg.processing.events.db.repository.event;

import ru.beorg.processing.events.db.model.event.DocumentEvent;
import ru.beorg.processing.events.db.search.document.DocumentSearchParameter;

import java.util.List;

public interface DocumentEventRepositoryCustom {

    List<DocumentEvent> findAll(DocumentSearchParameter searchParameters);

    int delete(DocumentSearchParameter searchParameters);

}
