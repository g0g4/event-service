package ru.beorg.processing.events.db.repository.event;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.beorg.processing.events.db.model.event.DocumentEvent;

public interface DocumentEventRepository extends MongoRepository<DocumentEvent, String>, DocumentEventRepositoryCustom {
}
