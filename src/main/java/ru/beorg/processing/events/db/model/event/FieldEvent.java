package ru.beorg.processing.events.db.model.event;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

import static ru.beorg.processing.events.db.model.MongoCollectionNames.FIELD_EVENT;

@Data
@Accessors(chain = true)
@Document(collection = FIELD_EVENT)
public class FieldEvent {

    @Id
    private String id;
    private Integer client;
    private String campaign;
    private Integer document;
    private String name;
    private Integer event;
    private String user;
    private String comment;
    private LocalDateTime date;
}
