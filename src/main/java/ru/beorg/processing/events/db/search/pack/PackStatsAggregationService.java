package ru.beorg.processing.events.db.search.pack;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.search.aggregation.AbstractAggregationService;
import ru.beorg.processing.events.db.search.aggregation.MongoGroupQueryBuilder;

import java.time.LocalDateTime;

import static ru.beorg.processing.events.db.model.MongoCollectionNames.PACK_EVENT;

@Service
public class PackStatsAggregationService extends AbstractAggregationService<PackSearchParameter> {

    @Autowired DateConverter dateConverter;

    public Iterable<DBObject> getStats(PackSearchParameter searchParameters) {
        return super.getStats(searchParameters, PACK_EVENT);
    }

    public int countEvents(PackSearchParameter searchParameters) {
        return super.countEvents(searchParameters, PACK_EVENT);
    }

    public boolean containsGroupByClauses(PackSearchParameter searchParameters) {
        return (searchParameters.getByClient() != null && searchParameters.getByClient()) ||
                (searchParameters.getByCampaign() != null && searchParameters.getByCampaign()) ||
                (searchParameters.getByDate() != null && searchParameters.getByDate()) ||
                (searchParameters.getByEvent() != null && searchParameters.getByEvent()) ||
                (searchParameters.getByShop() != null && searchParameters.getByShop()) ||
                (searchParameters.getByPack() != null && searchParameters.getByPack()) ||
                (searchParameters.getByUser() != null && searchParameters.getByUser());
    }

    protected void addGroupByClauses(MongoGroupQueryBuilder queryBuilder, PackSearchParameter searchParameters) {
        if (searchParameters.getByClient() != null && searchParameters.getByClient()) {
            queryBuilder.addGroupByField("client");
        }

        if (searchParameters.getByCampaign() != null && searchParameters.getByCampaign()) {
            queryBuilder.addGroupByField("campaign");
        }

        if (searchParameters.getByShop() != null && searchParameters.getByShop()) {
            queryBuilder.addGroupByField("shop");
        }

        if (searchParameters.getByPack() != null && searchParameters.getByPack()) {
            queryBuilder.addGroupByField("pack");
        }

        if (searchParameters.getByEvent() != null && searchParameters.getByEvent()) {
            queryBuilder.addGroupByField("event");
        }

        if (searchParameters.getByDate() != null && searchParameters.getByDate()) {
            queryBuilder.addGroupByField("date");
        }

        if (searchParameters.getByUser() != null && searchParameters.getByUser()) {
            queryBuilder.addGroupByField("user");
        }

        queryBuilder.addGroupByCountMethod("$sum", 1);
    }

    protected void addMatchClauses(MongoGroupQueryBuilder queryBuilder, PackSearchParameter searchParameters) {
        addMatchEqualsConditions(queryBuilder, searchParameters);
        addMatchInConditions(queryBuilder, searchParameters);
        addMatchGteLteConditions(queryBuilder, searchParameters);
    }

    protected void addProjectFields(MongoGroupQueryBuilder queryBuilder) {
        queryBuilder.addProjectField("count", 1);
        queryBuilder.addProjectField("group", "$_id");
        queryBuilder.addProjectField("_id", 0);
        queryBuilder.addProjectField("client", 1);
        queryBuilder.addProjectField("campaign", 1);
        queryBuilder.addProjectField("shop", 1);
        queryBuilder.addProjectField("pack", "$pack");
        queryBuilder.addProjectField("event", "$event");
        queryBuilder.addProjectField("user", "$user");
        queryBuilder.addProjectField("date", new BasicDBObject("$dateToString",
                new BasicDBObject("format", "%Y-%m-%d").append("date", "$date")));
    }

    protected void addFindClauses(MongoGroupQueryBuilder queryBuilder, PackSearchParameter searchParameters) {
        addFindEqConditions(queryBuilder, searchParameters);
        addFindInConditions(queryBuilder, searchParameters);
        addFindGteLteConditions(queryBuilder, searchParameters);
    }

    private void addMatchEqualsConditions(MongoGroupQueryBuilder queryBuilder, PackSearchParameter searchParameters) {
        if (searchParameters.getUser() != null) {
            queryBuilder.addMatchEqualsCondition("user", searchParameters.getUser());
        }

        if (searchParameters.getComment() != null) {
            queryBuilder.addMatchEqualsCondition("comment", searchParameters.getComment());
        }
    }

    private void addMatchInConditions(MongoGroupQueryBuilder queryBuilder, PackSearchParameter searchParameters) {
        if (searchParameters.getEvent() != null) {
            queryBuilder.addMatchInCondition("event", searchParameters.getEvent());
        }

        if (searchParameters.getClient() != null) {
            queryBuilder.addMatchInCondition("client", searchParameters.getClient());
        }

        if (searchParameters.getCampaign() != null) {
            queryBuilder.addMatchInCondition("campaign", searchParameters.getCampaign());
        }

        if (searchParameters.getShop() != null) {
            queryBuilder.addMatchInCondition("shop", searchParameters.getShop());
        }

        if (searchParameters.getPack() != null) {
            queryBuilder.addMatchInCondition("pack", searchParameters.getPack());
        }
    }

    private void addMatchGteLteConditions(MongoGroupQueryBuilder queryBuilder, PackSearchParameter searchParameters) {
        if (searchParameters.getDate() != null) {
            LocalDateTime date = dateConverter.convertDate(searchParameters.getDate()).toLocalDate().atStartOfDay();
            queryBuilder.addMatchGteCondition("date", dateConverter.toDate(dateConverter.toEpochSeconds(date)));
            queryBuilder.addMatchLteCondition("date", dateConverter.toDate(
                    dateConverter.toEpochSeconds(date.toLocalDate().plusDays(1).atStartOfDay())));
        } else {
            if (searchParameters.getStart() != null) {
                queryBuilder.addMatchGteCondition("date", dateConverter.toDate(searchParameters.getStart()));
            }

            if (searchParameters.getEnd() != null) {
                queryBuilder.addMatchLteCondition("date", dateConverter.toDate(searchParameters.getEnd()));
            }
        }
    }

    private void addFindEqConditions(MongoGroupQueryBuilder queryBuilder, PackSearchParameter searchParameters) {
        if (searchParameters.getUser() != null) {
            queryBuilder.addFindEqCondition("user", searchParameters.getUser());
        }

        if (searchParameters.getComment() != null) {
            queryBuilder.addFindEqCondition("comment", searchParameters.getComment());
        }
    }

    private void addFindInConditions(MongoGroupQueryBuilder queryBuilder, PackSearchParameter searchParameters) {
        if (searchParameters.getEvent() != null) {
            queryBuilder.addFindInCondition("event", searchParameters.getEvent());
        }

        if (searchParameters.getClient() != null) {
            queryBuilder.addFindInCondition("client", searchParameters.getClient());
        }

        if (searchParameters.getCampaign() != null) {
            queryBuilder.addFindInCondition("campaign", searchParameters.getCampaign());
        }

        if (searchParameters.getShop() != null) {
            queryBuilder.addFindInCondition("shop", searchParameters.getShop());
        }

        if (searchParameters.getPack() != null) {
            queryBuilder.addFindInCondition("pack", searchParameters.getPack());
        }
    }

    private void addFindGteLteConditions(MongoGroupQueryBuilder queryBuilder, PackSearchParameter searchParameters) {
        if (searchParameters.getDate() != null) {
            LocalDateTime date = dateConverter.convertDate(searchParameters.getDate()).toLocalDate().atStartOfDay();
            queryBuilder.addFindGteCondition("date", dateConverter.toDate(dateConverter.toEpochSeconds(date)));
            queryBuilder.addFindLteCondition("date", dateConverter.toDate(
                    dateConverter.toEpochSeconds(date.toLocalDate().plusDays(1).atStartOfDay())));
        } else {
            if (searchParameters.getStart() != null) {
                queryBuilder.addFindGteCondition("date", dateConverter.toDate(searchParameters.getStart()));
            }

            if (searchParameters.getEnd() != null) {
                queryBuilder.addFindLteCondition("date", dateConverter.toDate(searchParameters.getEnd()));
            }
        }
    }

}
