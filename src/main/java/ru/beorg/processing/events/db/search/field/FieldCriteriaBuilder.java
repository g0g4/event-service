package ru.beorg.processing.events.db.search.field;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;
import ru.beorg.processing.events.converter.DateConverter;

import java.time.LocalDateTime;

import static ru.beorg.processing.events.db.search.SearchParameterNames.CommonFields.*;
import static ru.beorg.processing.events.db.search.SearchParameterNames.DocumentFieldPackEventFields.*;
import static ru.beorg.processing.events.db.search.SearchParameterNames.DocumentFieldPackEventFields.FieldEventFields.DOCUMENT_ID;
import static ru.beorg.processing.events.db.search.SearchParameterNames.DocumentFieldPackEventFields.FieldEventFields.NAME;

@Component
public class FieldCriteriaBuilder {

    @Autowired DateConverter dateConverter;

    public Criteria build(FieldSearchParameter searchParameters) {
        Criteria criteria = new Criteria();

        addClientCriteria(criteria, searchParameters);
        addCampaignCriteria(criteria, searchParameters);
        addDocumentCriteria(criteria, searchParameters);

        if (searchParameters.getDate() != null) {
            addDateCriteria(criteria, searchParameters);
        } else if (searchParameters.getStart() != null || searchParameters.getEnd() != null) {
            addDateRangeCriteria(criteria, searchParameters);
        }

        addNameCriteria(criteria, searchParameters);
        addUserCriteria(criteria, searchParameters);
        addEventCriteria(criteria, searchParameters);
        addCommentCriteria(criteria, searchParameters);

        return criteria;
    }

    public Criteria addClientCriteria(Criteria criteria, FieldSearchParameter searchParameters) {
        if (searchParameters.getClient() != null) {
            criteria.and(CLIENT_ID).in(searchParameters.getClient());
        }

        return criteria;
    }

    public Criteria addCampaignCriteria(Criteria criteria, FieldSearchParameter searchParameters) {
        if (searchParameters.getCampaign() != null) {
            criteria.and(CAMPAIGN_ID).in(searchParameters.getCampaign());
        }

        return criteria;
    }

    public Criteria addDocumentCriteria(Criteria criteria, FieldSearchParameter searchParameters) {
        if (searchParameters.getDocument() != null) {
            criteria.and(DOCUMENT_ID).is(searchParameters.getDocument());
        }

        return criteria;
    }

    public Criteria addNameCriteria(Criteria criteria, FieldSearchParameter searchParameters) {
        if (searchParameters.getName() != null) {
            criteria.and(NAME).in(searchParameters.getName());
        }

        return criteria;
    }

    public Criteria addUserCriteria(Criteria criteria, FieldSearchParameter searchParameters) {
        if (searchParameters.getUser() != null) {
            criteria.and(USER).is(searchParameters.getUser());
        }
        return criteria;
    }

    public Criteria addEventCriteria(Criteria criteria, FieldSearchParameter searchParameters) {
        if (searchParameters.getEvent() != null) {
            criteria.and(EVENT).in(searchParameters.getEvent());
        }

        return criteria;
    }

    public Criteria addCommentCriteria(Criteria criteria, FieldSearchParameter searchParameters) {
        if (searchParameters.getComment() != null) {
            criteria.and(COMMENT).is(searchParameters.getComment());
        }

        return criteria;
    }

    public Criteria addDateCriteria(Criteria criteria, FieldSearchParameter searchParameters) {
        if (searchParameters.getDate() != null) {
            LocalDateTime date = dateConverter.convertDate(searchParameters.getDate()).toLocalDate().atStartOfDay();
            criteria.and(DATE).gte(date).lte(date.plusDays(1));
        }

        return criteria;
    }

    public Criteria addDateRangeCriteria(Criteria criteria, FieldSearchParameter searchParameters) {
        Criteria dateCriteria = Criteria.where(DATE);
        boolean isParameterPresent = false;

        if (searchParameters.getStart() != null) {
            dateCriteria.gte(dateConverter.convertDate(searchParameters.getStart()));
            isParameterPresent = true;
        }

        if (searchParameters.getEnd() != null) {
            dateCriteria.lte(dateConverter.convertDate(searchParameters.getEnd()));
            isParameterPresent = true;
        }

        if (isParameterPresent) {
            criteria.andOperator(dateCriteria);
        }

        return criteria;
    }

}
