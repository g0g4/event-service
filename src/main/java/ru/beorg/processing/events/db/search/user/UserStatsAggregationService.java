package ru.beorg.processing.events.db.search.user;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.search.aggregation.AbstractAggregationService;
import ru.beorg.processing.events.db.search.aggregation.MongoGroupQueryBuilder;

import java.time.LocalDateTime;

import static ru.beorg.processing.events.db.model.MongoCollectionNames.USER_EVENT;

@Service
public class UserStatsAggregationService extends AbstractAggregationService<UserSearchParameter> {

    @Autowired DateConverter dateConverter;

    public Iterable<DBObject> getStats(UserSearchParameter searchParameters) {
        return super.getStats(searchParameters, USER_EVENT);
    }

    public int countEvents(UserSearchParameter searchParameters) {
        return super.countEvents(searchParameters, USER_EVENT);
    }

    @Override
    public boolean containsGroupByClauses(UserSearchParameter searchParameters) {
        return (searchParameters.getByEvent() != null && searchParameters.getByEvent()) ||
                (searchParameters.getByUser() != null && searchParameters.getByUser()) ||
                (searchParameters.getByInitiator() != null && searchParameters.getByInitiator()) ||
                (searchParameters.getByDate() != null && searchParameters.getByDate());
    }

    @Override
    protected void addGroupByClauses(MongoGroupQueryBuilder queryBuilder, UserSearchParameter searchParameters) {
        if (searchParameters.getByEvent() != null && searchParameters.getByEvent()) {
            queryBuilder.addGroupByField("event");
        }

        if (searchParameters.getByUser() != null && searchParameters.getByUser()) {
            queryBuilder.addGroupByField("user");
        }

        if (searchParameters.getByInitiator() != null && searchParameters.getByInitiator()) {
            queryBuilder.addGroupByField("initiator");
        }

        if (searchParameters.getByDate() != null && searchParameters.getByDate()) {
            queryBuilder.addGroupByField("date");
        }

        queryBuilder.addGroupByCountMethod("$sum", 1);
    }

    @Override
    protected void addMatchClauses(MongoGroupQueryBuilder queryBuilder, UserSearchParameter searchParameters) {
        addMatchEqualsConditions(queryBuilder, searchParameters);
        addMatchInConditions(queryBuilder, searchParameters);
        addMatchGteLteConditions(queryBuilder, searchParameters);
    }

    @Override
    protected void addProjectFields(MongoGroupQueryBuilder queryBuilder) {
        queryBuilder.addProjectField("count", 1);
        queryBuilder.addProjectField("group", "$_id");
        queryBuilder.addProjectField("_id", 0);
        queryBuilder.addProjectField("event", "$event");
        queryBuilder.addProjectField("user", "$user");
        queryBuilder.addProjectField("initiator", "$initiator");
        queryBuilder.addProjectField("date", new BasicDBObject("$dateToString",
                new BasicDBObject("format", "%Y-%m-%d").append("date", "$date")));
    }

    @Override
    protected void addFindClauses(MongoGroupQueryBuilder queryBuilder, UserSearchParameter searchParameters) {
        addFindEqConditions(queryBuilder, searchParameters);
        addFindInConditions(queryBuilder, searchParameters);
        addFindGteLteConditions(queryBuilder, searchParameters);
    }

    private void addMatchEqualsConditions(MongoGroupQueryBuilder queryBuilder, UserSearchParameter searchParameters) {
        if (searchParameters.getUser() != null) {
            queryBuilder.addMatchEqualsCondition("user", searchParameters.getUser());
        }

        if (searchParameters.getInitiator() != null) {
            queryBuilder.addMatchEqualsCondition("initiator", searchParameters.getInitiator());
        }
    }

    private void addMatchInConditions(MongoGroupQueryBuilder queryBuilder, UserSearchParameter searchParameters) {
        if (searchParameters.getEvent() != null) {
            queryBuilder.addMatchInCondition("event", searchParameters.getEvent());
        }
    }

    private void addMatchGteLteConditions(MongoGroupQueryBuilder queryBuilder, UserSearchParameter searchParameters) {
        if (searchParameters.getDate() != null) {
            LocalDateTime date = dateConverter.convertDate(searchParameters.getDate()).toLocalDate().atStartOfDay();
            queryBuilder.addMatchGteCondition("date", dateConverter.toDate(dateConverter.toEpochSeconds(date)));
            queryBuilder.addMatchLteCondition("date", dateConverter.toDate(
                    dateConverter.toEpochSeconds(date.toLocalDate().plusDays(1).atStartOfDay())));
        }
    }

    private void addFindEqConditions(MongoGroupQueryBuilder queryBuilder, UserSearchParameter searchParameters) {
        if (searchParameters.getUser() != null) {
            queryBuilder.addFindEqCondition("user", searchParameters.getUser());
        }

        if (searchParameters.getInitiator() != null) {
            queryBuilder.addFindEqCondition("initiator", searchParameters.getInitiator());
        }
    }

    private void addFindInConditions(MongoGroupQueryBuilder queryBuilder, UserSearchParameter searchParameters) {
        if (searchParameters.getEvent() != null) {
            queryBuilder.addFindInCondition("event", searchParameters.getEvent());
        }
    }

    private void addFindGteLteConditions(MongoGroupQueryBuilder queryBuilder, UserSearchParameter searchParameters) {
        if (searchParameters.getDate() != null) {
            LocalDateTime date = dateConverter.convertDate(searchParameters.getDate()).toLocalDate().atStartOfDay();
            queryBuilder.addFindGteCondition("date", dateConverter.toDate(dateConverter.toEpochSeconds(date)));
            queryBuilder.addFindLteCondition("date", dateConverter.toDate(
                    dateConverter.toEpochSeconds(date.toLocalDate().plusDays(1).atStartOfDay())));
        }
    }

}
