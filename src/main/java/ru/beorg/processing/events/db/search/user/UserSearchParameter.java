package ru.beorg.processing.events.db.search.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class UserSearchParameter {

    private List<Integer> event;
    private String user;
    private String initiator;
    private Long date;

    @JsonProperty("by_date")
    private Boolean byDate;

    @JsonProperty("by_event")
    private Boolean byEvent;

    @JsonProperty("by_user")
    private Boolean byUser;

    @JsonProperty("by_initiator")
    private Boolean byInitiator;

}
