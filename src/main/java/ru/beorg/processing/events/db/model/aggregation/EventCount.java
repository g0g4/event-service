package ru.beorg.processing.events.db.model.aggregation;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class EventCount {
    private Integer event;
    private Long count;
}
