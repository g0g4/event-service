package ru.beorg.processing.events.db.search.pack;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import ru.beorg.processing.events.db.model.event.PackEvent;
import ru.beorg.processing.events.db.repository.event.PackEventRepositoryCustom;

import java.util.List;

public class PackEventRepositoryImpl implements PackEventRepositoryCustom {

    @Autowired MongoTemplate mongoTemplate;
    @Autowired PackCriteriaBuilder packCriteriaBuilder;

    @Override
    public List<PackEvent> findAll(PackSearchParameter searchParameters) {
        Criteria criteria = packCriteriaBuilder.build(searchParameters);

        return mongoTemplate.find(Query.query(criteria), PackEvent.class);
    }

    public int delete(PackSearchParameter searchParameters) {
        Criteria criteria = packCriteriaBuilder.build(searchParameters);

        return mongoTemplate.remove(Query.query(criteria), PackEvent.class).getN();
    }

}
