package ru.beorg.processing.events.db.search.field;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.search.aggregation.AbstractAggregationService;
import ru.beorg.processing.events.db.search.aggregation.MongoGroupQueryBuilder;

import java.time.LocalDateTime;

import static ru.beorg.processing.events.db.model.MongoCollectionNames.FIELD_AGGREGATION;

@Service
public class FieldStatsAggregationService extends AbstractAggregationService<FieldSearchParameter> {

    @Autowired DateConverter dateConverter;

    public Iterable<DBObject> getStats(FieldSearchParameter searchParameters) {
        return super.getStats(searchParameters, FIELD_AGGREGATION);
    }

    public int countEvents(FieldSearchParameter searchParameters) {
        return super.countEvents(searchParameters, FIELD_AGGREGATION);
    }

    @Override
    public boolean containsGroupByClauses(FieldSearchParameter searchParameters) {
        return (searchParameters.getByClient() != null && searchParameters.getByClient()) ||
                (searchParameters.getByCampaign() != null && searchParameters.getByCampaign()) ||
                (searchParameters.getByDate() != null && searchParameters.getByDate()) ||
                (searchParameters.getByEvent() != null && searchParameters.getByEvent());
    }

    @Override
    protected void addGroupByClauses(MongoGroupQueryBuilder queryBuilder, FieldSearchParameter searchParameters) {
        if (searchParameters.getByClient() != null && searchParameters.getByClient()) {
            queryBuilder.addGroupByField("client");
        }

        if (searchParameters.getByCampaign() != null && searchParameters.getByCampaign()) {
            queryBuilder.addGroupByField("campaign");
        }

        if (searchParameters.getByDate() != null && searchParameters.getByDate()) {
            queryBuilder.addGroupByField("date");
        }

        if (searchParameters.getByEvent() != null && searchParameters.getByEvent()) {
            queryBuilder.addGroupByField("event");
        }

        queryBuilder.addGroupByCountMethod("$sum", "$events.count");
    }

    @Override
    protected void addUnwindClauses(MongoGroupQueryBuilder queryBuilder) {
        queryBuilder.addUnwindField("events");
    }

    @Override
    protected void addProjectFields(MongoGroupQueryBuilder queryBuilder) {
        queryBuilder.addProjectField("events.count", 1);
        queryBuilder.addProjectField("count", "$events.count");
        queryBuilder.addProjectField("event", "$events.event");
        queryBuilder.addProjectField("group", "$_id");
        queryBuilder.addProjectField("_id", 0);
        queryBuilder.addProjectField("campaign", 1);
        queryBuilder.addProjectField("client", 1);
        queryBuilder.addProjectField("name", 1);
        queryBuilder.addProjectField("date", new BasicDBObject("$dateToString",
                new BasicDBObject("format", "%Y-%m-%d").append("date", "$date")));
    }

    @Override
    protected void addMatchClauses(MongoGroupQueryBuilder queryBuilder, FieldSearchParameter searchParameters) {
        addMatchEqualsConditions(queryBuilder, searchParameters);
        addMatchInConditions(queryBuilder, searchParameters);
        addMatchGteLteConditions(queryBuilder, searchParameters);
    }

    @Override
    protected void addFindClauses(MongoGroupQueryBuilder queryBuilder, FieldSearchParameter searchParameters) {
        addFindEqConditions(queryBuilder, searchParameters);
        addFindInConditions(queryBuilder, searchParameters);
        addFindGteLteConditions(queryBuilder, searchParameters);
    }

    private void addMatchEqualsConditions(MongoGroupQueryBuilder queryBuilder, FieldSearchParameter searchParameters) {
        if (searchParameters.getDocument() != null) {
            queryBuilder.addMatchEqualsCondition("document", searchParameters.getDocument());
        }

        if (searchParameters.getComment() != null) {
            queryBuilder.addMatchEqualsCondition("comment", searchParameters.getComment());
        }

        if (searchParameters.getUser() != null) {
            queryBuilder.addMatchEqualsCondition("user", searchParameters.getUser());
        }
    }

    private void addMatchInConditions(MongoGroupQueryBuilder queryBuilder, FieldSearchParameter searchParameters) {
        if (searchParameters.getEvent() != null) {
            queryBuilder.addMatchInCondition("events.event", searchParameters.getEvent());
            queryBuilder.addMatchAfterUnwindInCondition("event", searchParameters.getEvent());
        }

        if (searchParameters.getClient() != null) {
            queryBuilder.addMatchInCondition("client", searchParameters.getClient());
        }

        if (searchParameters.getCampaign() != null) {
            queryBuilder.addMatchInCondition("campaign", searchParameters.getCampaign());
        }

        if (searchParameters.getName() != null) {
            queryBuilder.addMatchInCondition("name", searchParameters.getName());
        }
    }

    private void addMatchGteLteConditions(MongoGroupQueryBuilder queryBuilder, FieldSearchParameter searchParameters) {
        if (searchParameters.getDate() != null) {
            LocalDateTime date = dateConverter.convertDate(searchParameters.getDate()).toLocalDate().atStartOfDay();
            queryBuilder.addMatchGteCondition("date", dateConverter.toDate(dateConverter.toEpochSeconds(date)));
            queryBuilder.addMatchLteCondition("date", dateConverter.toDate(
                    dateConverter.toEpochSeconds(date.toLocalDate().plusDays(1).atStartOfDay())));
        } else {
            if (searchParameters.getStart() != null) {
                queryBuilder.addMatchGteCondition("date", dateConverter.toDate(searchParameters.getStart()));
            }

            if (searchParameters.getEnd() != null) {
                queryBuilder.addMatchLteCondition("date", dateConverter.toDate(searchParameters.getEnd()));
            }
        }
    }

    private void addFindEqConditions(MongoGroupQueryBuilder queryBuilder, FieldSearchParameter searchParameters) {
        if (searchParameters.getDocument() != null) {
            queryBuilder.addFindEqCondition("document", searchParameters.getDocument());
        }

        if (searchParameters.getComment() != null) {
            queryBuilder.addFindEqCondition("comment", searchParameters.getComment());
        }

        if (searchParameters.getUser() != null) {
            queryBuilder.addFindEqCondition("user", searchParameters.getUser());
        }
    }

    private void addFindInConditions(MongoGroupQueryBuilder queryBuilder, FieldSearchParameter searchParameters) {
        if (searchParameters.getEvent() != null) {
            queryBuilder.addFindInCondition("events.event", searchParameters.getEvent());
        }

        if (searchParameters.getClient() != null) {
            queryBuilder.addFindInCondition("client", searchParameters.getClient());
        }

        if (searchParameters.getCampaign() != null) {
            queryBuilder.addFindInCondition("campaign", searchParameters.getCampaign());
        }

        if (searchParameters.getName() != null) {
            queryBuilder.addFindInCondition("name", searchParameters.getName());
        }
    }

    private void addFindGteLteConditions(MongoGroupQueryBuilder queryBuilder, FieldSearchParameter searchParameters) {
        if (searchParameters.getDate() != null) {
            LocalDateTime date = dateConverter.convertDate(searchParameters.getDate()).toLocalDate().atStartOfDay();
            queryBuilder.addFindGteCondition("date", dateConverter.toDate(dateConverter.toEpochSeconds(date)));
            queryBuilder.addFindLteCondition("date", dateConverter.toDate(
                    dateConverter.toEpochSeconds(date.toLocalDate().plusDays(1).atStartOfDay())));
        } else {
            if (searchParameters.getStart() != null) {
                queryBuilder.addFindGteCondition("date", dateConverter.toDate(searchParameters.getStart()));
            }

            if (searchParameters.getEnd() != null) {
                queryBuilder.addFindLteCondition("date", dateConverter.toDate(searchParameters.getEnd()));
            }
        }
    }

}
