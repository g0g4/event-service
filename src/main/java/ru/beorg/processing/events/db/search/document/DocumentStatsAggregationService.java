package ru.beorg.processing.events.db.search.document;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.beorg.processing.events.converter.DateConverter;
import ru.beorg.processing.events.db.search.aggregation.AbstractAggregationService;
import ru.beorg.processing.events.db.search.aggregation.MongoGroupQueryBuilder;

import static ru.beorg.processing.events.db.model.MongoCollectionNames.DOCUMENT_AGGREGATION;

@Service
public class DocumentStatsAggregationService extends AbstractAggregationService<DocumentSearchParameter> {

    @Autowired DateConverter dateConverter;

    public Iterable<DBObject> getStats(DocumentSearchParameter searchParameters) {
        return super.getStats(searchParameters, DOCUMENT_AGGREGATION);
    }

    public int countEvents(DocumentSearchParameter searchParameters) {
        return super.countEvents(searchParameters, DOCUMENT_AGGREGATION);
    }

    @Override
    public boolean containsGroupByClauses(DocumentSearchParameter searchParameters) {
        return (searchParameters.getByClient() != null && searchParameters.getByClient()) ||
                (searchParameters.getByCampaign() != null && searchParameters.getByCampaign()) ||
                (searchParameters.getByDate() != null && searchParameters.getByDate()) ||
                (searchParameters.getByEvent() != null && searchParameters.getByEvent());
    }

    @Override
    protected void addGroupByClauses(MongoGroupQueryBuilder queryBuilder, DocumentSearchParameter searchParameters) {
        if (searchParameters.getByClient() != null && searchParameters.getByClient()) {
            queryBuilder.addGroupByField("client");
        }

        if (searchParameters.getByCampaign() != null && searchParameters.getByCampaign()) {
            queryBuilder.addGroupByField("campaign");
        }

        if (searchParameters.getByDate() != null && searchParameters.getByDate()) {
            queryBuilder.addGroupByField("date");
        }

        if (searchParameters.getByEvent() != null && searchParameters.getByEvent()) {
            queryBuilder.addGroupByField("event");
        }

        queryBuilder.addGroupByCountMethod("$sum", "$events.count");
    }

    @Override
    protected void addUnwindClauses(MongoGroupQueryBuilder queryBuilder) {
        queryBuilder.addUnwindField("events");
    }

    @Override
    protected void addProjectFields(MongoGroupQueryBuilder queryBuilder) {
        queryBuilder.addProjectField("events.count", 1);
        queryBuilder.addProjectField("count", "$events.count");
        queryBuilder.addProjectField("event", "$events.event");
        queryBuilder.addProjectField("group", "$_id");
        queryBuilder.addProjectField("_id", 0);
        queryBuilder.addProjectField("campaign", 1);
        queryBuilder.addProjectField("shop", 1);
        queryBuilder.addProjectField("client", 1);
        queryBuilder.addProjectField("date", new BasicDBObject("$dateToString",
                new BasicDBObject("format", "%Y-%m-%d").append("date", "$date")));
    }

    @Override
    protected void addMatchClauses(MongoGroupQueryBuilder queryBuilder, DocumentSearchParameter searchParameters) {
        addMatchEqualsConditions(queryBuilder, searchParameters);
        addMatchInConditions(queryBuilder, searchParameters);
        addMatchGteLteConditions(queryBuilder, searchParameters);
    }

    @Override
    protected void addFindClauses(MongoGroupQueryBuilder queryBuilder, DocumentSearchParameter searchParameters) {
        addFindEqConditions(queryBuilder, searchParameters);
        addFindInConditions(queryBuilder, searchParameters);
        addFindGteLteConditions(queryBuilder, searchParameters);
    }

    private void addMatchEqualsConditions(MongoGroupQueryBuilder queryBuilder, DocumentSearchParameter searchParameters) {
        if (searchParameters.getDocument() != null) {
            queryBuilder.addMatchEqualsCondition("document", searchParameters.getDocument());
        }

        if (searchParameters.getComment() != null) {
            queryBuilder.addMatchEqualsCondition("comment", searchParameters.getComment());
        }

        if (searchParameters.getUser() != null) {
            queryBuilder.addMatchEqualsCondition("user", searchParameters.getUser());
        }
    }

    private void addMatchInConditions(MongoGroupQueryBuilder queryBuilder, DocumentSearchParameter searchParameters) {
        if (searchParameters.getEvent() != null) {
            queryBuilder.addMatchInCondition("events.event", searchParameters.getEvent());
            queryBuilder.addMatchAfterUnwindInCondition("event", searchParameters.getEvent());
        }

        if (searchParameters.getClient() != null) {
            queryBuilder.addMatchInCondition("client", searchParameters.getClient());
        }

        if (searchParameters.getCampaign() != null) {
            queryBuilder.addMatchInCondition("campaign", searchParameters.getCampaign());
        }

        if (searchParameters.getShop() != null) {
            queryBuilder.addMatchInCondition("shop", searchParameters.getShop());
        }
    }

    private void addMatchGteLteConditions(MongoGroupQueryBuilder queryBuilder, DocumentSearchParameter searchParameters) {
        if (searchParameters.getStart() != null) {
            queryBuilder.addMatchGteCondition("date", dateConverter.toDate(searchParameters.getStart()));
        }

        if (searchParameters.getEnd() != null) {
            queryBuilder.addMatchLteCondition("date", dateConverter.toDate(searchParameters.getEnd()));
        }
    }

    private void addFindEqConditions(MongoGroupQueryBuilder queryBuilder, DocumentSearchParameter searchParameters) {
        if (searchParameters.getDocument() != null) {
            queryBuilder.addFindEqCondition("document", searchParameters.getDocument());
        }

        if (searchParameters.getComment() != null) {
            queryBuilder.addFindEqCondition("comment", searchParameters.getComment());
        }

        if (searchParameters.getUser() != null) {
            queryBuilder.addFindEqCondition("user", searchParameters.getUser());
        }
    }

    private void addFindInConditions(MongoGroupQueryBuilder queryBuilder, DocumentSearchParameter searchParameters) {
        if (searchParameters.getEvent() != null) {
            queryBuilder.addFindInCondition("events.event", searchParameters.getEvent());
        }

        if (searchParameters.getClient() != null) {
            queryBuilder.addFindInCondition("client", searchParameters.getClient());
        }

        if (searchParameters.getCampaign() != null) {
            queryBuilder.addFindInCondition("campaign", searchParameters.getCampaign());
        }

        if (searchParameters.getShop() != null) {
            queryBuilder.addFindInCondition("shop", searchParameters.getShop());
        }
    }

    private void addFindGteLteConditions(MongoGroupQueryBuilder queryBuilder, DocumentSearchParameter searchParameters) {
        if (searchParameters.getStart() != null) {
            queryBuilder.addFindGteCondition("date", dateConverter.toDate(searchParameters.getStart()));
        }

        if (searchParameters.getEnd() != null) {
            queryBuilder.addFindLteCondition("date", dateConverter.toDate(searchParameters.getEnd()));
        }
    }

}
