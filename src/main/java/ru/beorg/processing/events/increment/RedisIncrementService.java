package ru.beorg.processing.events.increment;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import ru.beorg.processing.events.db.model.event.DocumentEvent;
import ru.beorg.processing.events.db.model.event.FieldEvent;

import static ru.beorg.processing.events.redis.Separator.SEPARATOR;

@Slf4j
@Service
public class RedisIncrementService {
    private static final String DOCUMENTS_DAILY = "documents_daily";
    private static final String FIELDS_DAILY = "fields_daily";

    @Autowired RedisTemplate<String, Long> redisTemplate;

    public void increment(DocumentEvent documentEvent){
        createOrIncrementRedisKey(DOCUMENTS_DAILY, createRedisKey(documentEvent));
    }

    public void increment(FieldEvent fieldEvent){
        createOrIncrementRedisKey(FIELDS_DAILY, createRedisKey(fieldEvent));
    }

    void createOrIncrementRedisKey(String hash, String redisKey) {
        HashOperations<String, String, Long> ops = redisTemplate.opsForHash();
        ops.increment(hash, redisKey, 1);
    }

    String createRedisKey(DocumentEvent documentEvent){
        StringBuilder key = new StringBuilder();
        key.append(documentEvent.getClient()).append(SEPARATOR);
        key.append(replaceSeparatorIfContains(documentEvent.getCampaign())).append(SEPARATOR);

        if (documentEvent.getShop() != null) {
            key.append(documentEvent.getShop());
        }

        key.append(SEPARATOR);
        key.append(documentEvent.getEvent()).append(SEPARATOR);
        key.append(documentEvent.getDate().toLocalDate());

        return key.toString();
    }

    String createRedisKey(FieldEvent fieldEvent){
        StringBuilder key = new StringBuilder();
        key.append(fieldEvent.getClient()).append(SEPARATOR);
        key.append(replaceSeparatorIfContains(fieldEvent.getCampaign())).append(SEPARATOR);
        key.append(fieldEvent.getEvent()).append(SEPARATOR);
        key.append(fieldEvent.getDate().toLocalDate());

        return key.toString();
    }
    
    String replaceSeparatorIfContains(String s){
        if(s.contains(SEPARATOR)){
            s = s.replaceAll(SEPARATOR, "");
            log.warn("The name of campaign was incorrect - replaced {}", s);
        }

        return s;
    }
}
