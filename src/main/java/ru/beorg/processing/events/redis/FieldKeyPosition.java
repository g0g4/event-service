package ru.beorg.processing.events.redis;

public interface FieldKeyPosition {
    int CLIENT = 0;
    int CAMPAIGN = 1;
    int EVENT = 2;
    int DATE =3;
}
