package ru.beorg.processing.events.redis;

public interface HashNames {
    String DOCUMENT_EVENT = "documents_daily";
    String FIELD_EVENT = "fields_daily";
}
