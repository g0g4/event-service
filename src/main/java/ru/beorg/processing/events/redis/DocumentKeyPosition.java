package ru.beorg.processing.events.redis;

public interface DocumentKeyPosition {
    int CLIENT = 0;
    int CAMPAIGN = 1;
    int SHOP = 2;
    int EVENT = 3;
    int DATE = 4;
}
