package ru.beorg.processing.events;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.beorg.processing.rabbit.common.autoconfigure.EnableBeorgQueuesCreation;
import ru.beorg.processing.rabbit.common.autoconfigure.EnableBeorgRabbit;

@EnableDiscoveryClient
@SpringBootApplication
@EnableBeorgRabbit
@EnableBeorgQueuesCreation
@EnableScheduling
public class EventsSystemApplication {

    @Value("${spring.redis.host}") String redisHost;
    @Value("${spring.redis.port}") String redisPort;

    public static void main(String[] args) {
        SpringApplication.run(new Object[]{EventsSystemApplication.class}, args);
    }

    @Bean
    public RedissonClient redissonClient() {
        Config config = new Config();
        config.useSingleServer().setAddress(redisHost + ":" + redisPort);
        return Redisson.create(config);
    }
}
